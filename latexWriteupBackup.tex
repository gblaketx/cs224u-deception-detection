%
% File acl2018.tex
%
%% Based on the style files for ACL-2017, with some changes, which were, in turn,
%% Based on the style files for ACL-2015, with some improvements
%%  taken from the NAACL-2016 style
%% Based on the style files for ACL-2014, which were, in turn,
%% based on ACL-2013, ACL-2012, ACL-2011, ACL-2010, ACL-IJCNLP-2009,
%% EACL-2009, IJCNLP-2008...
%% Based on the style files for EACL 2006 by 
%%e.agirre@ehu.es or Sergi.Balari@uab.es
%% and that of ACL 08 by Joakim Nivre and Noah Smith

\documentclass[11pt,a4paper]{article}
\usepackage[hyperref]{acl2018}
\usepackage{times}
\usepackage{latexsym}

\usepackage{multirow}
\usepackage{url}

\aclfinalcopy % Uncomment this line for the final submission TODO
%\def\aclpaperid{***} %  Enter the acl Paper ID here

%\setlength\titlebox{5cm}
% You can expand the titlebox if you need extra space
% to show all the authors. Please do not make the titlebox
% smaller than 5cm (the original size); we will check this
% in the camera-ready version and ask you to change it back.

\newcommand\BibTeX{B{\sc ib}\TeX}

\title{Generalizable Features for Deception Detection}

\author{Gordon Blake \\
  Department of Computer Science \\
  Stanford University \\
  {\tt gblake@stanford.edu} \\\And
  Jesse Burkett \\
  Symbolic Systems Program \\
  Stanford University\\
  {\tt jessepb@stanford.edu} \\}

\date{}

\begin{document}
\maketitle
\begin{abstract}
    Discerning between true and false statements, or deception detection, is an important problem in a variety of domains. We explore detecting deception in two datasets, one of truthful and deceptive hotel reviews and another of open domain true and false statements. We exceed previously published deception classification accuracy on both the reviews and open domain datasets using novel feature sets and feature extraction techniques. We test if deception detection models generalize across domains by training on the open domain dataset and testing on hotel reviews. We are unable to exceed change levels of accuracy, suggesting that learned feature weights do not transfer well.
    
\end{abstract}

\section{Introduction}
TODO

Discerning between true and false statements is an important problem in a variety of domains. Review sites like TripAdvisor and Yelp must filter out deceptive spam reviews to ensure users receive accurate information about hotels and restaurants. Courts must determine if testimony is admissible, particularly that of children who may have been "coached" to lie under pressure. In recent years, concern about fake news has increased calls for tools to establish the trustworthiness of online content.

All of these problems fall in the domain of deception detection. At its core, this is a binary classification task: given a set of statements, predict which are true and which are false. Our goal, then, is to contribute to this field in two ways. First, build classifiers that achieve high accuracy discerning between true and fabricated statements within multiple domains. Second, attempt to find generalizable features for deception detection that 

Can the weights learned from open domain deception apply in the more constrained area of deceptive hotel reviews?

We expand on previous work in this field first by achieving high accuracy on Ott et al.'s ~\shortcite{Ott:2011} and ~\shortcite{Ott:2013} gold-standard datasets. Specifically, we outperform the following three results: Xu and Zhao's 91.6\% accuracy on Ott et al.'s ~\shortcite{Ott:2011} positive review dataset~\shortcite{Xu:2012}, Ott et al.'s 86.0\% accuracy on the negative review dataset~\shortcite{Ott:2013}, and Ott et al.'s 87.2\% accuracy on the combined datasets~\shortcite{Ott:2013}. We expand successful techniques to open-domain deception detection datasets like the one used in Perez-Rosas and Mihalcea~\shortcite{Perez:2015} to achieve an accuracy gain of 3.5 percentage points over previously published results. Lastly, we attempt to extract general deception features by training classifiers using the Open Domain dataset and testing on TripAdvisor reviews. However, we fail to achieve accuracy exceeding chance in the cross-training regimen, suggesting that transfer of deception styles between domains may be limited.

\section{Prior Work}
TODO

Previous approaches to deception detection have examined a wide variety of features and model types in attempts to classify deceptive texts. 

TODO: reference Ott
In their experiments with these datasets, they found that bigram features in combination with features derived from 2007 Linguistic Inquiry and Word Count (LIWC) data provided the best indicators for correctly classifying deception among the positive reviews, while bigram features alone performed best while classifying the negative reviews. Both experiments also found a support vector machine (SVM) classifier to be the most effective model for this task. Other research \cite{Xu:2012,Feng:2012} used the datasets created by Ott et al. in their own experiments that test the performance of features and classifiers not described in Ott et al.’s papers. Xu and Zhao ~\shortcite{Xu:2012} looked to more structural features to improve performance on the Ott et al. \shortcite{Ott:2011} dataset. Notably, they implemented part-of-speech bigrams (BIPOS) and dependency parses (DEP) as features and trained a maximum entropy model (MEM) for the classification. Feng et al. ~\shortcite{Feng:2012} also utilized additional structural features, implementing deep syntax features based on Probabilistic Context Free Grammars (PCFGs). They got the best results from an SVM trained on deep syntax features combined with unigram features. 

    Outside the scope of hotel reviews, other research has attempted similar classification tasks in open-domain deception detection. Yancheva and Rudzicz~\shortcite{Yancheva:2013} found that syntactic features were especially helpful in the classification of their novel dataset. In particular, they noted that average sentence length, sentence complexity, and mean clauses per utterance were the most defining features for the success of their classifier. Many different model types were tested in this experiment, including linear regression (LR), multilayer perceptron (MLP), naïve Bayes (NB), random forest (RF), and SVM classifiers, with MLP and RF classifiers performing the best. Lastly, Perez-Rosas and Mihalcea~\shortcite{Perez:2015} used n-grams, syntactic features, semantic features, and readability/complexity features in their attempts at open-domain deception detection. This included shallow part-of-speech tags, deep syntax derived from PCFGs, and LIWC features. This experiment found that POS tags performed best, followed by PCFG features. This research also noted that words in LIWC categories "negation", "certain", and "you" occurred more often in deceptive texts than in truthful ones.
    
    Ultimately, previous research shows that a combination of syntactic and semantic features may yield the best results in the task of deception detection.

\begin{table*}
\centering
\begin{tabular}{|c|c|c|c|c|}
    \hline
    \bf Sentiment & \bf Model & \bf Features & \bf Source & \bf Accuracy \\ \hline \hline
    \multirow{2}{*}{Positive} & \multicolumn{2}{c|}{Human Judges} & \cite{Ott:2011} &\ 0.581 \\ \cline{2-5}
        
     & MaxEnt & Unigram + DEP & \cite{Xu:2012} & 0.916 \\ 
 \hline
    \multirow{2}{*}{Negative} & \multicolumn{2}{c|}{Human Judges} & \cite{Ott:2013} &  0.694 \\ \cline{2-5}
    & SVM & Bigrams$^+$ & \cite{Ott:2013} & 0.860 \\ \hline
    Combined & SVM & Bigrams$^+$ & \cite{Ott:2013} & 0.872 \\ \hline  
\end{tabular}
\caption{\label{baseline} Published human and machine performance on the TripAdvisor hotel reviews dataset \cite{Ott:2013}}
\end{table*}

% Note: I changed the Ott combined score to reflect accuracy testing on both positive and negative sentiment reviews rather than just the best subset. This is the proper comparison to make with our scores (which are better)

\section{Data}
We draw on two datasets for analysis, one of deceptive and truthful hotel reviews and another of "open domain" truths and lies.

\subsection{TripAdvisor}

Ott et al. produced two datasets that have been commonly used for testing the performance of classifiers in deception detection tasks. One consists of 400 truthful TripAdvisor reviews and 400 gold-standard deceptive positive hotel reviews \shortcite{Ott:2011} and one contains 400 truthful and 400 gold-standard deceptive negative reviews \shortcite{Ott:2013}.

\subsection{Open Domain}
TODO

Another deception dataset comes from the work of Perez-Rosas and Mihalcea~\shortcite{Perez:2015}. They elicited seven truths and seven lies from Amazon Mechanical Turk users, each of them consisting of a single sentence, resulting in 3584 truths and 3584 lies from 512 unique contributors. No topic was enforced, so the lies are "open domain." Lies include implausible statements such as "I have won the lottery 16 times" and factually incorrect assertions, as in "Google manufactures the iPhone." 

As with the TripAdvisor dataset, the process of lying was generative in that users made up lies rather than taking existing statements and altering them to make them false. The dataset might help reveal syntactic and semantic choices people make (perhaps unconsciously) when constructing lies.

Following the precedent of Perez-Rosas and Mihalcea, we collapse statements by user, so all of a user's false sentences form a single training example, as do all of a users true sentences.

\section{Model}
\label{sec:models}
In order to achieve high performance testing within datasets, we first explore a broader feature set and model space. We then use the best models in our cross-training regimen.

We extracted the following n-gram feature sets, with the features lowercased and lemmatized: Unigrams, Bigrams$^+$, Trigrams$^+$ where the superscript $^+$ indicates that the feature set subsumes the preceding feature set. We also experimented with using unstemmed and Porter stemmed n-gram tokens, but found that using lemmas gave better results across the board.

For syntactic features, we extracted part-of-speech tags to use both in unigram (POS) and bigram (BIPOS) feature sets. We extracted two types of POS tags: simple (e.g. VERB, NOUN) and detailed (e.g. VBZ, VBG, NN). We also extracted dependency parses DEP for each token in the reviews. We used the spaCy library for tokenization, lemmatization, and generating POS and DEP tags.

We also extracted "Deep Syntax" features by encoding counts of PCFG production rules as described by Feng et al.~\shortcite{Feng:2012}. As is in the original paper, we experimented with unlexicalized production rules (r), lexicalized production rules (r*), unlexicalized production rules combined with the grandparent node ($\hat{r}$), and lexicalized production rules combined with the grandparent node ($\hat{r}$*). We used the Stanford Parser to extract PCFG production rules~\cite{Klein:2003}.

Additionally, we extracted semantic features using the Linquistic Inquiry and Word Count (LIWC) lexicon~\cite{LIWC:2007}. For each document, we obtained counts on each of the 80 LIWC dimensions by obtaining the LIWC categories corresponding to each token in the document. Tokens were lowercased and lemmatized before lookup in the LIWC corpus, as we found that lemmatization led to higher precision and recall when using LIWC features in predictions.

% The efficacy of simple and detailed tags varied depending on model choice and whether POS or BIPOS features are used. Unless otherwise noted, the detailed tags were used in results reported here.

All count features are tf-idf normalized before being used for training. When using multiple count feature sets, we experimented with normalizing the sets separately, then combining them, and with normalizing the combined sets. We found that we typically achieved higher accuracy combining the feature sets and then normalizing, which is the technique used for results reported here.

\begin{table*}[t!]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|c|}
    \cline{4-9}
    \multicolumn{3}{c|}{} & \multicolumn{3}{c|}{Truthful} & \multicolumn{3}{c|}{Deceptive} \\
    \hline \bf Features & \bf Model & \bf Accuracy & \bf P & \bf R & \bf F & \bf P & \bf R & \bf F & \bf Average F1  \\
    \hline \hline
    POS & SVM$_{linear}$ & 0.746 & 0.77 & 0.71 & 0.74 & 0.73 & 0.78 & 0.76 & 0.75 \\ \hline
    LIWC & SVM$_{linear}$ & 0.763 & 0.75 & 0.78 & 0.77 & 0.77 & 0.75 & 0.76 & 0.76 \\ \hline
    Unigrams & SVM$_{linear}$ & 0.898 & 0.89 & 0.90 & 0.90 & 0.90 & 0.89 & 0.90 & 0.90 \\ \hline
    Bigrams$^+$ & SVM$_{linear}$ & \bf 0.914 & 0.93 & \bf 0.90 & \bf 0.91 & \bf 0.90 & 0.93 & \bf 0.91 & 0.\bf 91 \\ \hline
    LIWC+Bigrams$^+$ & SVM$_{linear}$ & 0.894 & 0.91 & 0.87 & 0.89 & 0.88 & 0.92 & 0.90 & 0.89 \\ \hline
    Trigrams$^+$ & SVM$_{linear}$ & 0.90 & 0.92 & 0.88 & 0.90 & 0.88 & 0.92 & 0.90 & 0.90 \\ \hline
    Unigrams & NB & 0.895 & 0.94 & 0.84 & 0.89 & 0.86 & 0.95 & 0.90 & 0.89 \\ \hline
    Bigrams$^+$ & NB & 0.887 & \bf 0.96 & 0.81 & 0.88 & 0.84 & 0.96 & 0.89 & 0.89 \\ \hline
    Trigrams$^+$ & NB & 0.889 & \bf 0.96 & 0.81 & 0.88 & 0.83 & \bf 0.97 & 0.90 & 0.89 \\
    \hline
\end{tabular}
\caption{\label{ott-positive} Replication of the results from Ott et al.~\shortcite{Ott:2011}}
\end{table*}

\begin{table*}[t!]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|c|}
    \cline{3-8}
        \multicolumn{2}{c|}{} & \multicolumn{3}{c|}{Truthful} & \multicolumn{3}{c|}{Deceptive} \\ \hline
    \bf Sentiment & \bf Accuracy & \bf P & \bf R & \bf F & \bf P & \bf R & \bf F & \bf Average F1 \\ \hline \hline
    Positive & 0.914 & 0.925 & 0.900 & 0.913 & 0.903 & 0.927 & 0.915 & 0.914 \\ \hline
    Negative & 0.875 & 0.889 & 0.858 & 0.873 & 0.862 & 0.892 & 0.877 & 0.875 \\ \hline
    Combined & 0.899 & 0.901 & 0.897 & 0.899 & 0.898 & 0.901 & 0.900 & 0.899 \\ \hline
\end{tabular}
\caption{\label{ott-negative} Replication of the results from Ott et al.~\shortcite{Ott:2013}}
\end{table*}

\subsection{Models}
We tested the following models: Support Vector Machine, both linear (SVM$_{linear}$) and non-linear (SVM$_{rbf}$), Naive Bayes (NB), Random Forest (RF), Maximum Entropy (MaxEnt), Multilayer Perceptron (MLP), and a logistic regression model trained with stochastic gradient descent (SGD). All models were implemented in Python using the scikit-learn library.

We performed hyperparameter tuning by training on a subset of folds of the TripAdvisor dataset using Bigrams$^+$ features for each model. We tuned the following parameters:
\begin{itemize}
\item SVM$_{linear}$: C, loss
\item SVM$_{rbf}$: C, gamma, tol %TODO: max iter
\item NB: alpha
\item RF: n\_estimators, min\_samples\_split
\item MaxEnt: penalty, C, solver
\item MLP: hidden\_layer\_sizes, solver
\item SGD: penalty, alpha, max\_iter
\end{itemize}

All code used in the project, including feature extractors and model hyperparameters, is available at \url{https://bitbucket.org/gblaketx/cs224u-deception-detection/src}

\subsection{Performance Standards}
We establish two standards of performance on the TripAdvisor dataset: a human baseline and the best classifier performance reported in the literature, shown in Table~\ref{baseline}. Human judges generally perform only slightly better than chance, and the accuracy results reported are from choosing the majority judgment of a panel of three judges. Human judgments on the combined dataset were not available. Because most published work has focused on the subset of reviews with positive sentiment, machine accuracy for this set is higher than the other sentiment divisions.

The best published performance on the Open Domain dataset is 69.5\% accuracy using a linear SVM trained with POS features~\cite{Perez:2015}.

Our baseline for performance in the cross-training condition is simply to perform statistically significantly above chance, or 50\% accuracy.

\section{Results}

\begin{table*}[t!]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|l|c|}
    \cline{5-10}
        \multicolumn{4}{c|}{} & \multicolumn{3}{c|}{Truthful} & \multicolumn{3}{c|}{Deceptive} \\ \hline
    \bf Dataset & \bf Model & \bf Features &  \bf Accuracy & \bf P & \bf R & \bf F & \bf P & \bf R & \bf F\\ \hline \hline
    TripAdvisor(+) & SVM$_{linear}$ & Bigrams$^+$ + r & 0.920 & 0.926 & 0.912 & 0.919 & 0.914 & 0.927 & 0.921 \\ \hline 
%     TODO: Negative numbers not yet updated
    TripAdvisor(-) & SGD & Bigrams$^+$ + r & 0.885 & TODO & 0.912 & 0.919 & 0.914 & 0.927 & 0.921 \\ \hline
    TripAdvisor & SVM$_{linear}$ & Bigrams$^+$ + r* & 0.904 & 0.902 & 0.907 & 0.905 & 0.907 & 0.901 & 0.904 \\ \hline
    OpenDomain & SVM$_{linear}$ & Bigrams$^+$ + DEP & 0.730 & 0.715 & 0.765 & 0.739 & 0.748 & 0.695 & 0.721 \\ \hline
    Cross-Training & SGD & POS + BIPOS & 0.551 & 0.547 & 0.6 & 0.572 & 0.557 & 0.502 & 0.52 \\ \hline
\end{tabular}
\caption{\label{best-results} Best results and models for each dataset. Average F1 scores were comparable to accuracy. (+) denotes positive sentiment reviews only and (-) negative sentiment reviews only.}
\end{table*}

Consistent with the literature~\cite{Ott:2011, Xu:2012, Ott:2013}, we report on precision, recall, and F1 scores as measures of performance. Combined F1 scores are macro-averaged, as all class sizes (truthful, deceptive, positive, negative) are the same. Because class sizes are balanced, and in order to compare with previous publications, we also report on accuracy. It can be argued that accuracy is an insufficient metric because there is no guarantee that truthful and spam reviews appear in equal proportion in the real world and may not have equal priority. For example, an automated spam detection system may prioritize a high F1 score on deceptive reviews. For this reason, we include F1 scores broken down by review class whenever possible.

Training and testing on each dataset individually, we achieve accuracy comparable to or better than the best published in the literature. However, we find that prediction accuracy when training on the Open Domain dataset and testing on TripAdvisor reviews is no better than chance. Our best results for each dataset are reported in Table~\ref{best-results}.

\subsection{TripAdvisor Results}
Employing our analysis pipeline, we were able to replicate the results from Ott et al.~\shortcite{Ott:2011} classifying on positive reviews only and using five-fold cross-validation with the same folds as in the original paper. Our results are reported in Table~\ref{ott-positive}. Differences between this table and Ott et al.'s work indicate the effect of our featurization pipeline, hyperparameter tuning, and model implementations on performance. In general, we find accuracy comparable to or better than the original paper. Because we lemmatized n-gram features (see section~\ref{ssec:key-features}), we find slightly better performance with those feature sets than in the original paper. Interestingly, we find that adding LIWC features to Bigrams$^+$ results in a performance drop, in contrast to the results from Ott et al.~\shortcite{Ott:2011}.

We also replicate the results from Ott et al.~\shortcite{Ott:2013} using SVM$_{linear}$ and Bigrams$^+$. Results are reported in Table~\ref{ott-negative}. Once again, we find that we somewhat outperform the accuracy from the paper (by at least 1.5 percentage points in each category).

Training and testing on the combined positive and negative datasets, we evaluated all of our models on each feature set individually as well as Unigrams and Bigrams$^+$ feature sets augmented with each of the POS, DEP, PCFG, and LIWC feature sets. We also experimented with combinations of three or more features sets, such as Bigrams$^+$ + DEP + POS and Bigrams$^+$ + DEP + LIWC. In general, we found that SVM$_{linear}$, MLP, and SGD outperformed other models, with SVM$_{linear}$ performing the best. Our best performance came from SVM$_{linear}$ with Bigrams$^+$ and PCFG features (type $r*$), with accuracy of 0.904 when testing with five-fold cross-validation. Interestingly, we found that adding additional feature sets tended to degrade performance. Combining all features did not result in improved performance.

\subsection{Open Domain Results}
TODO

As with the TripAdvisor Dataset, we evaluated all of our models on each feature set individually as well as combinations of feature sets (focusing on Unigrams and Bigrams$^+$ augmented with syntactic features).

TODO: Get results in

We achieved 73\% accuracy using Bigrams+ and DEP features with the SVM$_{linear}$ classifier. This is 3.5 percentage points higher than the best published by Perez-Rosas and Mihalcea~\shortcite{Perez:2015} who used neither bigram nor dependency features in their paper. Using DEP features gives an improvement of 1.2 percentage points in accuracy over Bigrams$^+$ alone, suggesting that the syntactic information provides utility beyond that of n-grams.

\subsection{Cross Training Results}
TODO

Due to computational constraints, we performed cross training only with the best models yet observed, SGD and SVC$_{linear}$. We again tested a wide variety of individual feature sets and their combinations. Hypothesizing that syntax may generalize better than n-gram features, which rely on specific words, we also tested combinations of syntax-only features, such as (POS + DEP) and non-n-gram features (POS + DEP + LIWC).

Perez-Rosas and Mihalcea~\shortcite{Perez:2015} note that in the Open Domain dataset, people appear to be less likely to lie when discussing positive experiences. To determine if review sentiment might be a confounding factor in the cross training, we experimented with testing on the positive and negative subsets of reviews individually. However, we still did not observe accuracy above chance levels.

TODO: Discuss focusing on SGD?

\section{Analysis}
TODO

\subsection{Models}
Consistent with the literature, we observe that the SVM$_{linear}$ tends to perform best. Logistic regression (SGD) and MLP achieve comparable, but slightly lower accuracy. In general, all three models had accuracy within 1 to 2 percentage points of each each other. Notably, MLP required much more processing time than the other two models to achieve comparable results. 

In addition to the hyperparameter tuning mentioned in section~\ref{sec:models}, we experimented with L1 regularization of the SVM$_{linear}$ and SGD models, particularly during Cross-Training experiments, hypothesizing that a sparser feature weight vector may generalize better. However, we found that L1 regularization typically led to slightly worse performance compared to L2-regularized models.

\subsection{Key Features}
\label{ssec:key-features}
Lemmatization explains much of our performance improvement using n-gram features compared to published results.

TODO: back this up with numbers

In general, adding features did not improve accuracy, although there were some exceptions to this observation. Combining Bigrams$^+$ with a single syntactic feature set, such as POS, DEP, or PCFG, tended to produce the best results. However, adding additional feature sets beyond this led to decreased performance. The observation that combining two semantic and syntactic feature sets improves accuracy is broadly consistent with the literature, although which syntax feature sets we useful varies somewhat from previous work.~\cite{Ott:2011,Feng:2012,Xu:2012}.

Using detailed over coarse representations of syntax features had mixed effects. In general, detailed POS features yielded better performance than simple tags. The POS results reported here used detailed tags. However, in some cases simple BIPOS features performed better than detailed BIPOS. It is possible that this is a result of the limited amount of data used for training, as detailed BIPOS may generate too many features to effectively learn the weights of.

combining PCFG rules with Bigrams produced the most consistent improvement of any non-n-gram feature set. In contrast with Feng et al.'s~\shortcite{Feng:2012} results, we found that including grandparent nodes (the $\hat{r}$ variants) generally did not increase accuracy. We did not find a clear pattern of the effect of including lexicalized rules ($r*$). As can be observed in Table~\ref{best-results}, both lexicalized and unlexicalized PCFG features contributed to top performance on different datasets.
TODO: discuss the utility of PCFGs in Open Domain

TODO: report best feature weights for TripAdvisor all Bigrams+ r in table

L1-based feature selection (using sklearn.feature\_selection.SelectFromModel) led to moderate accuracy gains, but the best classifiers did not rely on this feature selection step.

\subsection{Error Analysis}

\subsubsection{TripAdvisor}
TODO
We found that errors were equally likely on truthful and deceptive reviews.

Review sentiment was a relevant factor, as 59\% of classification errors occurred on negative reviews. However, there was no interaction between sentiment and truthful/deceptive class. Errors were equally likely on negative sentiment deceptive reviews and negative sentiment truthful reviews, and likewise for positive sentiment reviews.

We experimented with using gold review sentiment labels (positive or negative) as a feature set. However, we found that including sentiment reduces classification accuracy when testing on the entire dataset. This contrasts with the information presented in Ott et al.~\shortcite{Ott:2013} that sentiment interacts with deception techniques. This divergence, and the interaction of sentiment and deception, would be a fruitful area for future research. Perhaps strength of sentiment is as important as polarity, so including a continuous sentiment scale rather than a binary positive/negative feature could be useful. It is also possible that n-gram features effectively entail sentiment, so no novel information is added by the sentiment feature set.

\subsubsection{Open Domain}
TODO

Across our high-performing Open Domain models, we observed a slight bias toward classifying statements as truthful. For example, recall for truthful statements in the best performing model was 7 percentage points higher than recall for deceptive examples.

We performed content analysis 60 misclassified statements produced when training with SVM$_{linear}$ using Bigrams$^+$ and DEP features on a subset of the data. 33 had been erroneously predicted as truthful and 27 as deceptive. We found three types of statements: personal, factual, and cultural. Personal statements discussed aspects of the respondent's life, such as "I have red hair." or "My neighbors and I wave at each other when coming and going." As Perez-Rosas and Mihalcea~\shortcite{Perez:2015} discuss, these statements are difficult to classify because they may be true for one speaker and false for another. However, deceptive personal statements tend to be unusual or implausble, such as "I like to eat cereal with orange juice." or "I am a mother of 25 children." In contrast, truthful personal statements are often banal, like "I have three dogs." or "I just tried to kill a spider but it got away from me."

% Predict that humans would perform better at this task than TripAdvisor or automated classifier. Some related to IR tasks

% Three types of statements: personal, factual, cultural (aphorisms, lived experience, may have an element of subjectivity "Be cautious when you meet strangers"). Authors tend to use one type of statement for all seven

% factual statements, more for an IR system, as they may not contain very informative linguistic clues (ex. George Washington is on the one dollar bill and George Washington is on the five dollar bill are nearly identical, but one is true and one isn't)

% Although human performance has not been tested on the Open Domain dataset, 

% Analyzed 60 error statements made on a subset of Open Domain
% personal_knowledge: 35
% factual_knowledge: 21
% cultural_knowledge: 7

% Personal knowledge statements that are false have an air of implausibility (I won the lottery, I drive a horse drawn carriage.), while true ones tend to be more banal (My dog ate kibble this morning.), requires an understanding of what is likely/plausible

\subsubsection{Cross-Training}
Classifiers often made very unbalanced predictions, achieving 60 or 70\% recall on one class and 30 to 40\% on the other. Whether the favored class was deceptive or truthful varied depending on the feature set used, but bias in the direction of the truthful class was more frequent. Indeed, the principal driver of the best model's performance was that it balanced recall between classes, rather than making precise predictions.

When testing only on reviews of a single sentiment, we observed slightly better performance on positive than negative reviews.

\section{Conclusion}
Consistent with prior research, we find that automated deception detection classifiers are able to achieve high levels of performance, far exceeding human abilities, particularly in constrained domains. Even in the realm of open domain statements, a combination of semantic and syntactic features leads to accuracy far better than chance. However, in our cross-training regimen, we were unable to find evidence for deception features that can be learned in one domain and successfully transferred to another. These results cast doubt on some of the claims made about deceptive statements generally based on results from a single dataset~\cite{Ott:2011, Feng:2012}. However, much work remains to be done in the area of multi and open domain deception detection.

% \subsection{Remaining Work}

% There are a number of additional avenues we would like to pursue before our final paper. So far, our work has focused primarily on the subset of reviews that are positive. We would like to extend our analysis to see how the techniques we've tried so far perform on the negative and combined datasets.

% Additionally, we have performed only limited hand-tuning of hyperparameters so far. We would like to more systematically tune our models on a subset of folds using a grid search or randomized search technique.

% There are a number of additional feature sets we would like to explore. First, Probabilistic Context-Free Grammars (PCFGs) have been shown in the literature to be effective in deception detection~\cite{Feng:2012}. We would like to include this feature set in our testing. Up to this point, we have focused only on count-based features and would like to explore other types of features that holistically describe the reviews, such as a continuous representation of sentiment that encodes both the polarity (positive/negative) and strength of sentiment expressed in the text.

% As mentioned in Section~\ref{ssec:obstables}, we believe that reduced accuracy from adding additional features may be a product of overfitting. We would like to try several strategies to address this problem. First, we may perform dimensionality reduction on our feature sets by using an algorithm like Truncated SVD. Initial experiments suggest that this algorithm effectively reduces dimensionality while maintaining high levels of accuracy. Second, we would like to experiment with feature selection, from simple variance thresholds to selecting features based on importance weights in a model. We may also perform error analysis to better understand which features may be key discriminators.

% Finally, we can experiment with other modifications in our feature extraction pipeline. As mentioned in Section~\ref{ssec:milestones}, we currently td-idf normalize all count features. We may experiment with other normalization techniques such as those laid out by Leopold and Kinderman~\shortcite{Leopold:2002}. Other feature extraction modifications include trying a different dependency parser, such as the Stanford parser, which was used by Xu and Zhao~\shortcite{Xu:2012} or a larger spaCy reference corpus for lemmatization and POS tagging.

\bibliography{acl2018}
\bibliographystyle{acl_natbib}

\end{document}

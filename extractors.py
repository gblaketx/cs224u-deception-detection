from featurizers import *

from sklearn.base import BaseEstimator, TransformerMixin

class NGramExtractor(BaseEstimator, TransformerMixin):
    """Used to extract ngram features using featurizer"""

    def __init__(self, n):
        self.n = n
        print("[{}-grams]".format(n), end=' ')


    def transform(self, examples, y=None):

        # Uses lemmatization by default
        if self.n == 1:
            return [unigram_featurizer(ex) for ex in examples]
        elif self.n == 2:
            return [bigram_featurizer(ex) for ex in examples]
        elif self.n == 3: 
            return [trigram_featurizer(ex) for ex in examples]
        else:
            raise NotImplementedException(
                "NGram extractor not implemented for n = {}".format(self.n))


    def fit(self, examples, y=None):
        return self

class LIWCExtractor(BaseEstimator, TransformerMixin):

    def __init__(self):
        print("[LIWC]", end=' ')

    def transform(self, examples, y=None):
        return [LIWC_feats(ex) for ex in examples]


    def fit(self, examples, y=None):
        return self


class POSExtractor(BaseEstimator, TransformerMixin):

    def __init__(self, n, detailed):
        """
        n : whether to use unigrams (1) or bigrams (2). If n = '2+', uses combine unigrams and bigrams
        """
        print("[{}-POS detailed: {}]".format(n, detailed), end=' ')
        self.n = n
        self.detailed = detailed

    def transform(self, examples, y=None):
        if self.n == 1:
            return [POS_feats(ex, self.detailed) for ex in examples]
        elif self.n == 2:
            return [BIPOS_feats(ex, self.detailed) for ex in examples]
        elif self.n == '2+':
            return [BIPOS_feats(ex, self.detailed, combined=True) for ex in examples]
        else:
            raise NotImplementedException(
                "POS extractor not implemented for n = {}".format(self.n))

    def fit(self, examples, y=None):
        return self
      
class DEPExtractor(BaseEstimator, TransformerMixin):
    def __init__(self):
        print("[DEP]", end=' ')

    def transform(self, examples, y=None):
        return [DEP_feats(ex) for ex in examples]


    def fit(self, examples, y=None):
        return self

class SentimentExtractor(BaseEstimator, TransformerMixin):
    """Used to extract sentiment"""

    def __init__(self):
        pass

    def transform(self, examples, y=None):
        raise NotImplementedException('SentimentExtractor has not yet been implemented')
        # return [{"<sentiment>" : "positive"} for _ in examples]


    def fit(self, examples, y=None):
        return self

class SentimentExtractorOracle(SentimentExtractor):
    """Simulates sentiment extraction by returning the true label"""

    def transform(self, examples, y=None):
        # sent = [{"<sentiment>" : sentiment_dict[ex]} for ex in examples]
        # print(list(zip(examples, sent)))
        # return sent

        #Note: would need global in settings file for this to work
        return [{"<sentiment>" : sentiment_dict[ex]} for ex in examples]

        # Adds the sentiment and the hotel as additional features
        # res = []
        # for ex in examples:
        #     sent, hotel, label = extras_dict[ex]
        #     feats = {
        #         "<sentiment>" : sent,
        #         "<hotel>" : hotel
        #     }
        #     res.append(feats)
        # return res



# class PCFGExtractor(BaseEstimator, TransformerMixin):

#     def __init__(self, lexicalized, include_gp, dataset):
#         """
#         rules : whether to use unlexicalized or lexicalized production rules.
#             false = unlexicalized production rules - all production rules exc those with terminal nodes
#                 e.g. NP -> NP SBAR, but not PRP -> 'you'
#             true = lexicalized production rules - all production rules

#         include_gp : whether or not to include grandparent nodes within the features (bool value)

#         """
#         print("[lexicalized rules: {}, including grandparent nodes: {}]".format(lexicalized, include_gp), end=' ')
#         if lexicalized is None:
#             raise ValueError("Expected lexicalized to be boolean, is None")

#         self.lex = lexicalized
#         self.gp = include_gp
#         self.dataset = dataset

#     def transform(self, examples, y=None):
#         # return [{"feat" : 1} for ex in examples]
#         return [PCFG_feats_cached(ex, self.lex, self.gp, self.dataset) for ex in examples]

#     def fit(self, examples, y=None):
#         return self

class PCFGExtractor(BaseEstimator, TransformerMixin):

    def __init__(self, lex, gp, dataset):
        """
        n : whether to use unigrams (1) or bigrams (2). If n = '2+', uses combine unigrams and bigrams
        """
        print("[PCFG Lex: {}, GP: {}]".format(lex, gp), end=' ')
        self.lex = lex
        self.gp = gp
        self.dataset = dataset

    def transform(self, examples, y=None):
        return [PCFG_feats_cached(ex, self.lex, self.gp, self.dataset) for ex in examples]

    def fit(self, examples, y=None):
        return self
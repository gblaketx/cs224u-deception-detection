import settings
import os
import pickle

from featurizers import PCFG_feats_all_types #PCFG_feats, PCFG_feats_OLD

rand_state = 42

def main():
    """
    Running this function parses all entries for both the Ott and OpenDomain datasets and stores the results
    in a set of pickled dictionaries in the "pcfg_parses" directory
    """
    settings.init()
    parse_all_PCFGs()

def parse_all_PCFGs():
    """
    Saves eight dicts as pickles

    ott_no_lex_no_gp
    ott_lex_no_gp
    ott_no_lex_gp
    ott_lex_gp

    open_domain_no_lex_no_gp
    open_domain_lex_no_gp
    open_domain_no_lex_gp
    open_domain_lex_gp

    each dictionary is organized as follows:
    {
        "text of example" : <ParseCounter>

    }
    
    each example text is a key mapping to a dict with four keys in the form of (lex, gp)
    """


    settings.init()
    parse_dicts = {
        "ott" : {
            "ott_no_lex_no_gp" : {},
            "ott_lex_no_gp" : {},
            "ott_no_lex_gp" : {},
            "ott_lex_gp" : {}
        }, 
        "open_domain" : {
            "open_domain_no_lex_no_gp" : {},
            "open_domain_lex_no_gp" : {},
            "open_domain_no_lex_gp" : {},
            "open_domain_lex_gp" : {}
        }
    }

    parse_ott(parse_dicts["ott"])
    save_dicts(parse_dicts, "ott")
    parse_open_domain(parse_dicts["open_domain"])
    save_dicts(parse_dicts, "open_domain")

def save_dicts(parse_dicts, dataset_name):
    data_home = "pcfg_parses"
    for dict_name in parse_dicts[dataset_name]:
        savepath = os.path.join(data_home, dict_name + '.pickle')
        with open(savepath, 'wb') as outfile:
            pickle.dump(parse_dicts[dataset_name][dict_name], outfile)  

def parse_ott(ott_dicts):
    df = settings.loader.load("ott", polarity="all", fold="all")
    examples = df["text"].tolist()

    for ex in examples:
        feats_dict = PCFG_feats_all_types(ex)
        ott_dicts["ott_no_lex_no_gp"][ex] = feats_dict["no_lex_no_gp"]
        ott_dicts["ott_lex_no_gp"][ex] = feats_dict["lex_no_gp"] 
        ott_dicts["ott_no_lex_gp"][ex] = feats_dict["no_lex_gp"] 
        ott_dicts["ott_lex_gp"][ex] = feats_dict["lex_gp"]

def parse_open_domain(open_domain_dicts):
    df = settings.loader.load("open_domain", splits="all", collapse_users=True)[0] #Note, parses ONLY exist for collapsed users
    examples = df["text"].tolist()

    for ex in examples:
        feats_dict = PCFG_feats_all_types(ex)
        open_domain_dicts["open_domain_no_lex_no_gp"][ex] = feats_dict["no_lex_no_gp"]
        open_domain_dicts["open_domain_lex_no_gp"][ex] = feats_dict["lex_no_gp"]
        open_domain_dicts["open_domain_no_lex_gp"][ex] = feats_dict["no_lex_gp"] 
        open_domain_dicts["open_domain_lex_gp"][ex] = feats_dict["lex_gp"] 

def testLoad(filename="ott_lex_gp"):
    filepath = os.path.join("pcfg_parses", filename + '.pickle')
    with open(filepath, 'rb') as infile:
        res_dict = pickle.load(infile)
        print(res_dict)

if __name__ == '__main__':
    main()
    # testLoad()
    # testLoad("open_domain_no_lex_no_gp")

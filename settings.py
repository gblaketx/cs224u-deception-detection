import DataLoader

def init():
    """
    Initializes a DataLoader that can be globally referenced throughout files
    """
    global loader
    loader = DataLoader.DataLoader()

import pandas as pd
import os
import LIWC

from sklearn.model_selection import train_test_split

rand_state = 42

class DataLoader():

    data_home = "data"
    ott_path = os.path.join(data_home, "op_spam_v1.4")
    open_deception_path = os.path.join(data_home, "OpenDeception", "7Truth7LiesDataset.csv")

    def __init__(self):
        self.cache = {"ott": {}, "open_domain" : {}}


    def load(self, name, polarity=None, fold=None, splits=None, collapse_users=True):
        """
        Returns a dataframe of the given information
        """
        if name == "ott":
            return self.loadOtt(polarity, fold)
        elif name == "open_domain":
            return self.loadOpenDeception(splits, collapse_users)
        else:
            raise RuntimeError("Unrecognized dataset " + name)


    def loadOpenDeception(self, splits, collapse_users):
        """
        splits: list of "train", "dev", and "test" or "all"
        """

        if splits == "all":
            key = (splits, collapse_users)
            if key in self.cache["open_domain"]: 
                return self.cache["open_domain"][key]

        # key = tuple(splits + [collapse_users])
        # if key in self.cache["open_domain"]:
            # return self.cache["open_domain"][key]
            #Note: Cache if needed

        data = pd.read_csv(DataLoader.open_deception_path, quotechar="'", escapechar='\\')

        # self.cache[key] = data

        if collapse_users:
            res_classes = []
            res_text = []

            current_id = None
            current_class = None
            text_list = []

            for _, row in data.iterrows():
                id = row["id"].split('_')[0]

                if id == current_id:
                    if current_class == row["class"]:
                        text_list.append(row["text"])
                    else:
                        if len(text_list) > 0:
                            res_text.append(' '.join(text_list))
                            res_classes.append(current_class)
                        current_class = row["class"]
                        text_list = []

                else:
                    current_id = id
                    if len(text_list) > 0:
                        res_text.append(' '.join(text_list))
                        res_classes.append(current_class)
                    current_class = row["class"]
                    text_list = []

            res_dict = {'class': res_classes, 'text' : res_text}
            data = pd.DataFrame(res_dict)
            data.to_csv("test.csv")
            # print(data)

        if splits == "all": 
            self.cache["open_domain"][key] = [data]
            return [data] # In list to confirm with spec

        train, rest = train_test_split(data, test_size=0.4, random_state=rand_state)

        dev, test = train_test_split(rest, test_size=0.5, random_state=rand_state)


        data_splits = {
            'train': train,
            'dev' : dev,
            'test': test
        }

        res = []
        if not type(splits) is list:
            raise ValueError("Expected list type for splits, got {}".format(type(splits)))
        for name in splits:
            res.append(data_splits[name])

        return res

    # def split

    def loadOtt(self, polarity, fold):
        """
        polarity: polarity of the reviews to load. positive, negative, or all.
        fold: which fold to load. 1 through 5 or all. May be an integer or a list of integers
        """

        if fold == "all":
            fold_list = list(range(1,6))
        elif type(fold) == list:
            fold_list = fold
        else:
            fold_list = [fold]

        key = tuple(fold_list + [polarity]) #Note: This could be done more efficiently with folds
        if key in self.cache["ott"]:
            return self.cache["ott"][key]
            

        if polarity == "positive":
            res = self.loadOttHelper(
                os.path.join(DataLoader.ott_path, "positive_polarity"), fold_list)
        elif polarity == "negative":
            res = self.loadOttHelper(
                os.path.join(DataLoader.ott_path, "negative_polarity"), fold_list)
        elif polarity == "all":
            res = self.loadOttHelper(
                os.path.join(DataLoader.ott_path, "positive_polarity"), fold_list)

            res = res.append(self.loadOttHelper(
                os.path.join(DataLoader.ott_path, "negative_polarity"), fold_list))
        else:
            raise RuntimeError("Unrecognized polarity " + polarity)


        #Note: May want to shuffle rows on loading
        self.cache["ott"][key] = res
        return res

    def loadOttHelper(self, path, folds):
        """
        path: filepath to load
        fold: list of integers corresponding to which folds to load
        """
        filenames = []
        text = []
        labels = []
        example_folds = []


        polarity_name = os.path.basename(path).split('_')[0]

        for label in ["deceptive", "truthful"]:
            for fold in folds:
                dirpath = os.path.join(path, label, "fold{}".format(fold))
                for filename in os.listdir(dirpath):
                    filenames.append(filename[:-4]) # Remove .txt extension
                    with open(os.path.join(dirpath, filename), 'r') as file:
                        filetext = file.read()
                        text.append(filetext)
                        labels.append(label)
                        example_folds.append(fold)

        res_dict = {"filename": filenames, "text": text, "label": labels, 
                "fold": example_folds, "polarity": [polarity_name for _ in range(len(filenames))]}
        return pd.DataFrame(res_dict)


    def loadLIWC(self):
        if 'LIWC' in self.cache: return self.cache['LIWC']

        liwc = LIWC.LIWC()
        self.cache['LIWC'] = liwc
        
        return liwc







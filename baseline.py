import settings

import array
import numpy as np
import scipy.sparse as sp

from models import *
from sklearn.model_selection import cross_val_predict

def main():
    settings.init()
    ottBaseline(polarity="positive")


def crossTrainExperiment(pipeline_func, model, train="open_domain", polarity="all", collapse_users=True, verbose=False, **kwargs):
    open_domain_df = settings.loader.load('open_domain', splits="all", collapse_users=collapse_users)[0]
    open_domain_text, y_open_domain = split_open_domain_examples(open_domain_df)

    ott_df = settings.loader.load('ott', polarity=polarity, fold="all")
    ott_text, y_ott = split_ott_examples(ott_df)

    if train == "open_domain": # Train on open domain, test on ott
        train_text = open_domain_text
        y_train = y_open_domain
        test_text = ott_text
        y_test = y_ott
    elif train == "ott":
        train_text = ott_text
        y_train = y_ott
        test_text = open_domain_text
        y_test = y_open_domain  
    else:
        raise ValueError("Unrecognized training datset name {}".format(train))

    kwargs['dataset'] = 'both' #Used for PCFG extraction

    print("Cross train experiment, train dataset: {}, polarity: {}".format(train, polarity))
    mod = pipeline_func(train_text, y_train, model, kwargs)


    print("")
    train_predictions = mod.predict(train_text)
    if verbose:
        print("Train Accuracy: {}".format(accuracy_score(y_train, train_predictions)))

    predictions = mod.predict(test_text)
    print("Accuracy: {}".format(accuracy_score(y_test, predictions)))
    print(classification_report(y_test, predictions, digits=3))

    if verbose:
        feature_names = mod.named_steps['vect'].get_feature_names()
        coefs = mod.named_steps['clf'].coef_[0]
        zipped = zip(list(coefs), feature_names)
        zipped = zip(list(coefs), feature_names)
        print(sorted(zipped, key = lambda t: t[0]))


    return mod

def openDomainExperiment(pipeline_func, model, splits=["train", "dev"], collapse_users=True, verbose=False, **kwargs):
    train_df, dev_df = settings.loader.load('open_domain', splits=splits, collapse_users=collapse_users)
    train_text, y_train = split_open_domain_examples(train_df)
    dev_text, y_dev = split_open_domain_examples(dev_df)

    print("OpenDomain Experiment")
    kwargs["dataset"] = "open_domain"
    mod = pipeline_func(train_text, y_train, model, kwargs)
    predictions = mod.predict(dev_text)
    print("")
    print("Accuracy: {}".format(accuracy_score(y_dev, predictions)))
    print(classification_report(y_dev, predictions, digits=3))

    if verbose: analzyeErrors(y_dev, predictions, dev_text)

    return mod

def openDomainFiveFold(pipeline_func, model, collapse_users=True, verbose=False, **kwargs):
    data_df = settings.loader.load('open_domain', splits="all", collapse_users=collapse_users)[0]
    data_text, y_data = split_open_domain_examples(data_df)


    kwargs['fit'] = False # Tells the pipeline function not to fit
    kwargs["dataset"] = "open_domain"
    print("OpenDomain Experiment")
    mod = pipeline_func(None, None, model, kwargs)

    predictions = cross_val_predict(mod, data_text, y_data, cv=5)
    print("")
    print("Accuracy: {}".format(accuracy_score(y_data, predictions)))
    print(classification_report(y_data, predictions, digits=3))

def ottExperiment(pipeline_func, model, polarity="positive", train_folds=[1,2,3], dev_fold=4, verbose=False, **kwargs):
    """
    pipeline_func: function that returns a model

    model: the model to use, which can be added to a pipeline and supports a fit function

    polarity: the polarity of the examples to test on: "positive", "negative", or "all"

    train_folds: an integer or list of integers denoting the folds to use for training

    dev_fold: an integer or list of integers denoting the folds to use for testing

    """

    ott_train_df = settings.loader.load("ott", polarity=polarity, fold=train_folds)
    ott_dev_df = settings.loader.load("ott", polarity=polarity, fold=dev_fold)

    train_text, y_train = split_ott_examples(ott_train_df)
    dev_text, y_dev = split_ott_examples(ott_dev_df)


    kwargs["dataset"] = "ott"
    mod = pipeline_func(train_text, y_train, model, kwargs)
    predictions = mod.predict(dev_text)
    print("")
    print("Accuracy: {}".format(accuracy_score(y_dev, predictions)))
    print(classification_report(y_dev, predictions, digits=3))

    if verbose: 
        analzyeErrors(y_dev, predictions, dev_text, sentiment=ott_dev_df['polarity'].tolist())
        # feature_names = mod.named_steps['vect'].get_feature_names() #Note: This won't work in all cases
        # coefs = mod.named_steps['clf'].coef_[0]
        # zipped = zip(list(coefs), feature_names)
        # print(sorted(zipped, key = lambda t: t[0]))


    return mod

def ottFiveFold(pipeline_func, model, polarity="positive", verbose=False, **kwargs):
    """
    Tests on the ott et al baseline with five-fold cross-validation.
    Because this includes all the data, use this framework for TESTING ONLY, not development

    model_func: Function that takes in a list of training examples and training labels
    and returns a model that .fit can be called on with test examples and labels
    see mod_2gram_svm_baseline for an example
    polarity: the polarity of the examples to test on: "positive", "negative", or "all"

    Prints the classification report from combining all predictions
    """

    print("Five-fold cross-validation {} polarity".format(polarity))
    ott_df_all = settings.loader.load("ott", polarity=polarity, fold="all")
    all_predictions = []
    all_actual = []
    sent = []
    all_test_text = []

    kwargs["dataset"] = "ott" #Says which dataset to use when loading PCFG features
    folds = list(range(1, 6))
    for test_fold in folds:
        train_folds = folds[:test_fold-1] + folds[test_fold:]
        train_text, y_train = split_ott_examples(ott_df_all[ott_df_all["fold"].isin(train_folds)])
        test_text, y_test = split_ott_examples(ott_df_all[ott_df_all["fold"] == test_fold])

        if verbose: 
            all_test_text.extend(test_text)
            sent.extend(ott_df_all[ott_df_all["fold"] == test_fold]['polarity'].tolist())

        mod = pipeline_func(train_text, y_train, model, kwargs)
        print("")
        
        all_predictions.extend(mod.predict(test_text)) #Have to extend b/c result is numpy array
        all_actual += y_test

    print("Accuracy: {}".format(accuracy_score(all_actual, all_predictions)))
    print(classification_report(all_actual, all_predictions, digits=3))

    if verbose:
        analzyeErrors(all_actual, all_predictions, all_test_text, sentiment=sent)


def ottBaseline(polarity):
    print("Ott et al. Bigram SVM Baseline {} polarity".format(polarity))
    # Reproduces Ott et al.'s 2011 performance using BIGRAMS+ and SVM
    # Five-fold cross-validation using Ott et al.'s folds
    return ottFiveFold(mod_ngram, Models().get('svm'), ngrams=2)


def split_ott_examples(df):
    return (df["text"].tolist(), df["label"].tolist())

def split_open_domain_examples(df):
    return (df["text"].tolist(), df["class"].tolist())


def analzyeErrors(actual, predictions, data, sentiment=None):
    # sent_counts = {"positive": 0, "negative": 0}

    sent_counts = {"deceptive": {"positive": 0, "negative": 0}, "truthful": {"positive": 0, "negative": 0}}
    for i, act in enumerate(actual):
        pred = predictions[i]
        if act != pred:
            if sentiment is None:
                print("Predicted: {}, Actual: {}\nText: {}".format(pred, act, data[i]))
            else:
                # sent_counts[sentiment[i]] += 1
                sent_counts[pred][sentiment[i]] += 1
                # print(i)
                print("Predicted: {}, Actual: {}, Sentiment:{}\nText: {}".format(pred, act, sentiment[i], data[i]))
    if not sentiment is None: 
        print("Errors by sentiment:")
        print("Predicted    deceptive   truthful")
        print("positive     {}          {}".format(sent_counts["deceptive"]["positive"], sent_counts["truthful"]["positive"]))
        print("negative     {}          {}".format(sent_counts["deceptive"]["negative"], sent_counts["truthful"]["negative"]))

        print(sent_counts)

if __name__ == "__main__":
    main()


### DEPRECATED ###
# def ottExperiment(model_func, polarity="positive", train_folds=[1,2,3], dev_fold=4):
#     """
#     model_func: Function that takes in a list of training examples and training labels
#     and returns a a tuple  a model that .fit can be called 
#     on with test examples and labels. See mod_unigram_nb for an example

#     polarity: the polarity of the examples to test on: "positive", "negative", or "all"

#     train_folds: an integer or list of integers denoting the folds to use for training

#     dev_fold: an integer or list of integers denoting the folds to use for testing

#     """

#     ott_train_df = loader.load("ott", polarity=polarity, fold=train_folds)
#     ott_dev_df = loader.load("ott", polarity=polarity, fold=dev_fold)

#     train_text, y_train = split_ott_examples(ott_train_df)
#     dev_text, y_dev = split_ott_examples(ott_dev_df)

#     mod = model_func(train_text, y_train)
#     predictions = mod.predict(dev_text)
#     print(classification_report(y_dev, predictions))

#     return mod

# def ottFiveFold(model_func, polarity="positive"):
#     """
#     Tests on the ott et al baseline with five-fold cross-validation.
#     Because this includes all the data, use this framework for TESTING ONLY, not development

#     model_func: Function that takes in a list of training examples and training labels
#     and returns a model that .fit can be called on with test examples and labels
#     see mod_2gram_svm_baseline for an example

#     polarity: the polarity of the examples to test on: "positive", "negative", or "all"

#     Prints the classification report from combining all predictions
#     """

#     print("Five-fold cross-validation {} polarity".format(polarity))
#     ott_df_all = loader.load("ott", polarity=polarity, fold="all")
#     all_predictions = []
#     all_actual = []

#     folds = list(range(1, 6))
#     for test_fold in folds:
#         train_folds = folds[:test_fold-1] + folds[test_fold:]
#         train_text, y_train = split_ott_examples(ott_df_all[ott_df_all["fold"].isin(train_folds)])
#         test_text, y_test = split_ott_examples(ott_df_all[ott_df_all["fold"] == test_fold])
#         mod = model_func(train_text, y_train)
        
#         all_predictions.extend(mod.predict(test_text)) #Have to extend b/c result is numpy array
#         all_actual += y_test

#     print(classification_report(all_actual, all_predictions))

# def ottExperimentSentimentOracle(model_func, polarity="positive", train_folds=[1,2,3], dev_fold=4):
#     """
#     model_func: Function that takes in a list of training examples and training labels
#     and returns a model that .fit can be called on with test examples and labels
#     see mod_2gram_svm_baseline for an examplee model to use, which can be added to a pipeline and supports a fit function

#     polarity: the polarity of the examples to test on: "positive", "negative", or "all"

#     train_folds: an integer or list of integers denoting the folds to use for training

#     dev_fold: an integer or list of integers denoting the folds to use for testing

#     """

#     ott_train_df = settings.loader.load("ott", polarity=polarity, fold=train_folds)
#     ott_dev_df = settings.loader.load("ott", polarity=polarity, fold=dev_fold)

#     train_text, y_train = split_ott_examples(ott_train_df)
#     dev_text, y_dev = split_ott_examples(ott_dev_df)

#     def add_to_sent_dict(df):
#         for _, row in df.iterrows():
#             sentiment_dict[row["text"]] = row["polarity"]
#             # label = row["filename"].split('_')[0] #Puts the label there, for true oracle
#             # hotel = row["filename"].split('_')[1]
#             # extras = (row["polarity"], hotel)
#             # extras_dict[row["text"]] = extras

#     add_to_sent_dict(ott_train_df)
#     add_to_sent_dict(ott_dev_df)

#     mod = model_func(train_text, y_train)
#     predictions = mod.predict(dev_text)
#     print(classification_report(y_dev, predictions))

#     return mod
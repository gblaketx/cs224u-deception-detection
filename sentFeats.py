import settings
import os
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.sentiment.util import*
from models import *
from sklearn.base import BaseEstimator, TransformerMixin
from baseline import*

vader = SentimentIntensityAnalyzer()


def main():
	settings.init()
	models = Models()
	#ottFiveFold(mod_ngram, models.get("linear_svm"), ngrams=2)
	
	#ottFiveFold(mod_ngram_pcfg, models.get('linear_svm'), collapse_users=True, ngrams=2, lexicalized=True, include_gp=False)
	#ottFiveFold(mod_ngram_pcfg_sent_score, models.get('linear_svm'), collapse_users=True, ngrams=2, lexicalized=True, include_gp=False)


	#ottFiveFold(mod_ngram_sent_score, models.get("linear_svm"), polarity='negative', ngrams=1)
	#ottFiveFold(mod_ngram, models.get("linear_svm"), polarity='negative', ngrams=1)

	ottFiveFold(mod_ngram_sent_score, models.get("linear_svm"), polarity='all', ngrams=2)
	ottFiveFold(mod_ngram, models.get("linear_svm"), polarity='all', ngrams=2)

	#openDomainExperiment(mod_ngram_sent_score, models.get('linear_svm'), ngrams=1)
	#openDomainExperiment(mod_ngram, models.get('linear_svm'), ngrams=1)

	#ottFiveFold(mod_ngram_sent_score, models.get("sgd"), polarity='all', ngrams=1)
	#ottFiveFold(mod_ngram, models.get("sgd"), polarity='all', ngrams=1)
	openDomainFiveFold(mod_ngram_sent_score_od, models.get('sgd'), ngrams=2)
	openDomainFiveFold(mod_ngram, models.get('sgd'), ngrams=2)

	#openDomainFiveFold(mod_ngram_sent_score, models.get('sgd_l1'), ngrams=2 )
	#openDomainFiveFold(mod_ngram_sent_score, models.get('random_forest'), ngrams=2 )
	#openDomainFiveFold(mod_ngram_sent_score, models.get('logit'), ngrams=2 )
	#openDomainFiveFold(mod_ngram_sent_score, models.get('mlp'), ngrams=2 )

	openDomainFiveFold(mod_ngram_sent_score_od, models.get('linear_svm'), ngrams=2 )
	openDomainFiveFold(mod_ngram, models.get('linear_svm'), ngrams=2)

	#openDomainFiveFold(mod_ngram, models.get('sgd'), ngrams=2)
	#openDomainFiveFold(mod_ngram_dep, models.get('svm'), ngrams=2)
	#openDomainFiveFold(mod_ngram_dep, models.get('linear_svm'), ngrams=2)

def mod_ngram_sent_score(train_text, y_train, model, kwargs):
	print("Features: ", end='')
	if 'ngrams' in kwargs:
		ngrams = kwargs['ngrams']
	else:
		raise ValueError('Must pass number of ngrams to model')
	
	count_pipe = Pipeline([
		('ngrams', extractors.NGramExtractor(ngrams)),
		('vect', DictVectorizer()),
		('tfidf', TfidfTransformer())])
	sent_score_pipe = Pipeline([
		('sent_score_feats', SentimentScoreExtractor()),
		('vect', DictVectorizer())])
	mod = Pipeline([
		('feats', FeatureUnion([
			('count_feats', count_pipe),
			('sent_score_feats', sent_score_pipe),
		])),
		('clf', model)
	])
	mod.fit(train_text, y_train)
	return mod

def mod_ngram_pcfg_sent_score(train_text, y_train, model, kwargs):
	print("Features: ", end='')
	if 'ngrams' in kwargs:
		ngrams = kwargs['ngrams']
	else:
		raise ValueError('Must pass number of ngrams to model')

	if 'lexicalized' in kwargs:
		lexicalized = kwargs['lexicalized']
	else:
		raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

	if 'include_gp' in kwargs:
		include_gp = kwargs['include_gp']
	else:
		raise ValueError('Must pass in whether to use grandparent rules')

	if 'dataset' in kwargs:
		dataset = kwargs['dataset']
	else:
		raise ValueError('Must specify which dataset to load')

	count_pipe = Pipeline([
		('ngrams', extractors.NGramExtractor(ngrams)),
		('vect', DictVectorizer()),
		('tfidf', TfidfTransformer())])
	pcfg_pipe = Pipeline([
        ('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
        ('vect', DictVectorizer())])
	sent_score_pipe = Pipeline([
		('sent_score_feats', SentimentScoreExtractor()),
		('vect', DictVectorizer())])
	mod = Pipeline([
		('feats', FeatureUnion([
			('count_feats', count_pipe),
			('pcfg_feats', pcfg_pipe),
			('sent_score_feats', sent_score_pipe)
		])),
		('clf', model)
	])
	mod.fit(train_text, y_train)
	return mod


def mod_ngram_sent_score_od(train_text, y_train, model, kwargs):
	print("Features: ", end='')
	if 'ngrams' in kwargs:
		ngrams = kwargs['ngrams']
	else:
		raise ValueError('Must pass number of ngrams to model')
	
	count_pipe = Pipeline([
		('ngrams', extractors.NGramExtractor(ngrams)),
		('vect', DictVectorizer()),
		('tfidf', TfidfTransformer())])
	sent_score_pipe = Pipeline([
		('sent_score_feats', SentimentScoreExtractor()),
		('vect', DictVectorizer())])
	mod = Pipeline([
		('feats', FeatureUnion([
			('count_feats', count_pipe),
			('sent_score_feats', sent_score_pipe),
		])),
		('clf', model)
	])
	#mod.fit(train_text, y_train)
	return mod
		
class SentimentScoreExtractor(BaseEstimator, TransformerMixin):
	def __init__(self):
		print("[Sentiment Score]", end=' ')

	def transform(self, examples, y=None):
		return [sent_score_feats(ex) for ex in examples]

	def fit(self, examples, y=None):
		return self

def sent_score_feats(text):
	score = vader.polarity_scores(text)
	#pol = 1 if score['neg'] < score['pos'] else 0
	#score['pol'] = pol
	score = {'pos':score['pos'], 'neg':score['neg'], 'neu':score['neu']}
	return score


if __name__ == '__main__':
	main()
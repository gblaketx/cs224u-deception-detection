
Model: linear_svm
Five-fold cross-validation all polarity
Features: [2-grams] [1-POS detailed: True] 
Features: [2-grams] [1-POS detailed: True] 
Features: [2-grams] [1-POS detailed: True] 
Features: [2-grams] [1-POS detailed: True] 
Features: [2-grams] [1-POS detailed: True] 
Accuracy: 0.89
             precision    recall  f1-score   support

  deceptive      0.891     0.889     0.890       800
   truthful      0.889     0.891     0.890       800

avg / total      0.890     0.890     0.890      1600


Model: linear_svm
Five-fold cross-validation all polarity
Features: [2-grams] 
Features: [2-grams] 
Features: [2-grams] 
Features: [2-grams] 
Features: [2-grams] 
Accuracy: 0.898125
             precision    recall  f1-score   support

  deceptive      0.901     0.895     0.898       800
   truthful      0.896     0.901     0.898       800

avg / total      0.898     0.898     0.898      1600


Model: linear_svm
Five-fold cross-validation all polarity
Features: [2-grams] [2+-POS detailed: True] 
Features: [2-grams] [2+-POS detailed: True] 
Features: [2-grams] [2+-POS detailed: True] 
Features: [2-grams] [2+-POS detailed: True] 
Features: [2-grams] [2+-POS detailed: True] 
Accuracy: 0.88625
             precision    recall  f1-score   support

  deceptive      0.883     0.890     0.887       800
   truthful      0.889     0.882     0.886       800

avg / total      0.886     0.886     0.886      1600


Model: linear_svm
Five-fold cross-validation all polarity
Features: [2-grams] [DEP] 
Features: [2-grams] [DEP] 
Features: [2-grams] [DEP] 
Features: [2-grams] [DEP] 
Features: [2-grams] [DEP] 
Accuracy: 0.88625
             precision    recall  f1-score   support

  deceptive      0.889     0.882     0.886       800
   truthful      0.883     0.890     0.887       800

avg / total      0.886     0.886     0.886      1600


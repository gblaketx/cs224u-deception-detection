
import extractors


from sklearn.pipeline import Pipeline, FeatureUnion

# Import sklearn models
from sklearn.svm import SVC, LinearSVC
from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier, LogisticRegression, LassoCV
from sklearn.ensemble import RandomForestClassifier, VotingClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.feature_selection import VarianceThreshold, SelectPercentile, SelectFromModel, SelectKBest
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.decomposition import PCA, TruncatedSVD

# Feature Vectorizers
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.feature_extraction import DictVectorizer

# Hyperparameter Optimization
from skopt import BayesSearchCV
from skopt.space import Categorical, Real, Integer
from sklearn.metrics import classification_report, accuracy_score 

rand_state = 42
# sentiment_dict = {} #Note Uncomment global when using sentiment extractor gold


class Models():

    def __init__(self):
        self.models = { 
            'svm' : lambda: SVC(C=2.218284056104764e-05, cache_size=200, class_weight=None, coef0=0.0,
                      decision_function_shape='ovr', degree=4, gamma=1.041150082612475e-05,
                      kernel='rbf', max_iter=-1, probability=False, random_state=rand_state,
                      shrinking=True, tol=0.001, verbose=False),

            'linear_svm' : lambda: LinearSVC(C=1.5, random_state=rand_state, 
                            loss="squared_hinge"),

            'nb' : lambda: MultinomialNB(alpha=0.062),

            'random_forest' : lambda: RandomForestClassifier(n_estimators=500,
                                min_samples_split=10,
                                random_state=rand_state),

            'logit' : lambda: LogisticRegression(penalty='l2', C=2, 
                solver='liblinear', random_state=rand_state),

            'mlp' : lambda: MLPClassifier(hidden_layer_sizes=(100,100), 
                        random_state=rand_state, solver='lbfgs'),


            'sgd' : lambda: SGDClassifier(loss='log', penalty='l2',
                        alpha=1e-4, random_state=rand_state, max_iter=8),

            'sgd_l1' : lambda: SGDClassifier(loss='log', penalty='l1',
                alpha=1e-4, random_state=rand_state, max_iter=8)

        }

    def get(self, name):
        print("\nModel: {}".format(name))
        return self.models[name]()


# NGram Model Wrappers

def mod_ngram(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    
    print("Features: ", end='')
    mod = Pipeline([('ngrams', extractors.NGramExtractor(ngrams)),
                    ('vect', DictVectorizer()),
                    ('tfidf', TfidfTransformer()),
                    ('clf', model)])
    
    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_ngram_feature_select(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    print("Features: ", end='')
    mod = Pipeline([('ngrams', extractors.NGramExtractor(ngrams)),
                    ('vect', DictVectorizer()),
                    ('tfidf', TfidfTransformer()),
                    ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))),
                    ('clf', model)])
    
    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod


def mod_ngram_pos_svd(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer()),
        ('lsa', TruncatedSVD(n_components=200, random_state=rand_state))])

    pos_pipe = Pipeline([
        ('liwc', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer()),
        ('lsa', TruncatedSVD(n_components=20, random_state=rand_state))]) #First, try individual dimreduce

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pos_feats', pos_pipe)
        ])),
        ('tfidf', TfidfTransformer()), #Note: Can experiment with other transform types
        ('clf', model)
    ])

    mod.fit(train_text, y_train) 
    return mod

def mod_ngram_pos_modselect(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('pos', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pos_feats', pos_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('feature_selection', SelectFromModel(LinearSVC(C=1.5))),
        ('clf', model)
    ])

    # Running feature selection after joint tf-idf transform seems to do okay
    # Feature selecting then transforming seems to not do well
    # transforming and feature selecting individually seems to do worse
    # Feature selecting only ngrams (with individual tf-idf transforms) performs worse
    # Feature selecting only ngrams then tdidf transforming performs worse

    # More aggressive feature selection seems to help weaker models like naive bayes

    mod.fit(train_text, y_train) 
    return mod

def mod_ngram_liwc(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    liwc_pipe = Pipeline([
        ('liwc', extractors.LIWCExtractor()),
        ('vect', DictVectorizer())])
        # ('feat_select', SelectKBest())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('liwc_feats', liwc_pipe)
        ])),
        ('tfidf', TfidfTransformer()), # Doing a single TFIDF transform seems to be better than individual
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod


    mod.fit(train_text, y_train) 
    return mod


def mod_ngram_pos(train_text, y_train, model, kwargs):
    """
    Models with ngrams and pos
    """
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('pos', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())]) 

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pos_feats', pos_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        # ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train) 
    return mod    

def mod_ngram_pos_feature_select(train_text, y_train, model, kwargs):
    """
    Models with ngrams and pos
    """
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('pos', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())]) 

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pos_feats', pos_pipe)
        ])),
        ('tfidf', TfidfTransformer()), #Note: Can experiment with other transform types
        ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train) 
    return mod    


def mod_ngram_pcfg(train_text, y_train, model, kwargs):

    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'lexicalized' in kwargs:
        lexicalized = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        include_gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    if 'dataset' in kwargs:
        dataset = kwargs['dataset']
    else:
        raise ValueError('Must specify which dataset to load')

    print("Features: ", end='')

    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pcfg_pipe = Pipeline([
        ('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pcfg_feats', pcfg_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_ngram_pcfg_feature_select(train_text, y_train, model, kwargs):

    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'lexicalized' in kwargs:
        lexicalized = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        include_gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    if 'dataset' in kwargs:
        dataset = kwargs['dataset']
    else:
        raise ValueError('Must specify which dataset to load')

    print("Features: ", end='')

    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pcfg_pipe = Pipeline([
        ('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pcfg_feats', pcfg_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_ngram_dep_pcfg(train_text, y_train, model, kwargs):

    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'lexicalized' in kwargs:
        lexicalized = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        include_gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    if 'dataset' in kwargs:
        dataset = kwargs['dataset']
    else:
        raise ValueError('Must specify which dataset to load')

    print("Features: ", end='')

    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pcfg_pipe = Pipeline([
        ('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('dep_feats', dep_pipe),
            ('pcfg_feats', pcfg_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_ngram_dep(train_text, y_train, model, kwargs):
    """
    Models with ngrams and dependency parses
    """
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('dep_feats', dep_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train) 
    return mod   

def mod_ngram_dep_feature_select(train_text, y_train, model, kwargs):
    """
    Models with ngrams and dependency parses
    """
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('dep_feats', dep_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train) 
    return mod  

def mod_ngram_dep_pos(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('liwc', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())]) 


    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('dep_feats', dep_pipe),
            ('pos_feats', pos_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_ngram_dep_liwc(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    liwc_pipe = Pipeline([
        ('liwc', extractors.LIWCExtractor()),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('dep_feats', dep_pipe),
            ('liwc_feats', liwc_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_ngram_pos_liwc(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')


    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    liwc_pipe = Pipeline([
        ('liwc', extractors.LIWCExtractor()),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('pos', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())]) 

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pos_feats', pos_pipe),
            ('liwc_feats', liwc_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_dep(train_text, y_train, model, kwargs):
    """
    Only features are dependency parse counts
    """
    print("Features: ", end='')
    mod = Pipeline([('dep', extractors.DEPExtractor()),
                    ('vect', DictVectorizer()),
                    ('tfidf', TfidfTransformer()),
                    # ('feature_selection', SelectFromModel(LinearSVC(C=1.5))),
                    ('clf', model)])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_dep_pcfg(train_text, y_train, model, kwargs):
    """
    Only features are dependency parse counts and PCFG rules
    """
    if 'lexicalized' in kwargs:
        lexicalized = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        include_gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    if 'dataset' in kwargs:
        dataset = kwargs['dataset']
    else:
        raise ValueError('Must specify which dataset to load')

    print("Features: ", end='')

    pcfg_pipe = Pipeline([
        ('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('dep_feats', dep_pipe),
            ('pcfg_feats', pcfg_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_liwc(train_text, y_train, model, kwargs):
    """
    Only features are LIWC counts
    """
    print("Features: ", end='')
    mod = Pipeline([('dep', extractors.LIWCExtractor()),
                    ('vect', DictVectorizer()),
                    ('tfidf', TfidfTransformer()),
                    ('clf', model)])
    
    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_pos(train_text, y_train, model, kwargs):
    """
    Only features are POS counts
    """

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    print("Features: ", end='')
    mod = Pipeline([('dep', extractors.POSExtractor(posgrams, detailed)),
                    ('vect', DictVectorizer()),
                    ('tfidf', TfidfTransformer()),
                    ('clf', model)])
    
    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_pos_pcfg(train_text, y_train, model, kwargs):
    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    if 'lexicalized' in kwargs:
        lexicalized = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        include_gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    if 'dataset' in kwargs:
        dataset = kwargs['dataset']
    else:
        raise ValueError('Must specify which dataset to load')

    print("Features: ", end='')
    pcfg_pipe = Pipeline([
        ('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('pos', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())]) 

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('pcfg_feats', pcfg_pipe),
            ('pos_feats', pos_pipe)
        ])),
        ('tfidf', TfidfTransformer()), 
        # ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train) 
    return mod  

def mod_ngram_pos_pcfg(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    if 'lexicalized' in kwargs:
        lexicalized = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        include_gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    if 'dataset' in kwargs:
        dataset = kwargs['dataset']
    else:
        raise ValueError('Must specify which dataset to load')

    print("Features: ", end='')
    pcfg_pipe = Pipeline([
        ('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
        ('vect', DictVectorizer())])

    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('pos', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())]) 

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pcfg_feats', pcfg_pipe),
            ('pos_feats', pos_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        # ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train) 
    return mod  

def mod_pcfg(train_text, y_train, model, kwargs):

    if 'lexicalized' in kwargs:
        lexicalized = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        include_gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    if 'dataset' in kwargs:
        dataset = kwargs['dataset']
    else:
        raise ValueError('Must specify which dataset to load')

    print("Features: ", end='')
    mod = Pipeline([('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
                    ('vect', DictVectorizer()),
                    ('tfidf', TfidfTransformer()),
                    ('clf', model)])
    
    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_pcfg_feature_select(train_text, y_train, model, kwargs):
    if 'lexicalized' in kwargs:
        lex = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    print("Features: ", end='')
    mod = Pipeline([('pcfg', extractors.PCFGExtractor(lex, gp)),
                    ('vect', DictVectorizer()),
                    ('tfidf', TfidfTransformer()),
                    ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))),
                    ('clf', model)])
    
    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod


def mod_dep_liwc(train_text, y_train, model, kwargs):
    print("Features: ", end='')
    liwc_pipe = Pipeline([
        ('liwc', extractors.LIWCExtractor()),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('dep_feats', dep_pipe),
            ('liwc_feats', liwc_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train)
    return mod

def mod_syntax(train_text, y_train, model, kwargs):
    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')
    
    if 'lexicalized' in kwargs:
        lexicalized = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        include_gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    if 'dataset' in kwargs:
        dataset = kwargs['dataset']
    else:
        raise ValueError('Must specify which dataset to load')

    print("Features: ", end='')
    pos_pipe = Pipeline([
        ('pos', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    pcfg_pipe = Pipeline([
        ('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('pos_feats', pos_pipe),
            ('dep_feats', dep_pipe),
            ('pcfg_feats', pcfg_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train) 
    return mod  


def mod_non_ngram(train_text, y_train, model, kwargs):
    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    print("Features: ", end='')
    liwc_pipe = Pipeline([
        ('liwc', extractors.LIWCExtractor()),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('pos', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('pos_feats', pos_pipe),
            ('liwc_feats', liwc_pipe),
            ('dep_feats', dep_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train) 
    return mod  

def mod_all(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    if 'lexicalized' in kwargs:
        lexicalized = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        include_gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    if 'dataset' in kwargs:
        dataset = kwargs['dataset']
    else:
        raise ValueError('Must specify which dataset to load')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('liwc', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())])

    liwc_pipe = Pipeline([
        ('liwc', extractors.LIWCExtractor()),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    pcfg_pipe = Pipeline([
        ('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pos_feats', pos_pipe),
            ('dep_feats', dep_pipe),
            ('liwc_feats', liwc_pipe),
            ('pcfg_feats', pcfg_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train) 
    return mod  


def mod_all_feature_select(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    if 'lexicalized' in kwargs:
        lexicalized = kwargs['lexicalized']
    else:
        raise ValueError('Must pass in what kinds rules you want to model (True for lexicalized, False for unlexicalized)')

    if 'include_gp' in kwargs:
        include_gp = kwargs['include_gp']
    else:
        raise ValueError('Must pass in whether to use grandparent rules')

    if 'dataset' in kwargs:
        dataset = kwargs['dataset']
    else:
        raise ValueError('Must specify which dataset to load')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('liwc', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())])

    liwc_pipe = Pipeline([
        ('liwc', extractors.LIWCExtractor()),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    pcfg_pipe = Pipeline([
        ('pcfg', extractors.PCFGExtractor(lexicalized, include_gp, dataset)),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pos_feats', pos_pipe),
            ('dep_feats', dep_pipe),
            ('liwc_feats', liwc_pipe),
            ('pcfg_feats', pcfg_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))),
        ('clf', model)
    ])

    if 'fit' in kwargs:
        if not kwargs['fit']: return mod

    mod.fit(train_text, y_train) 
    return mod  
# Models incorporating ngrams and sentiment

def mod_ngram_sent(train_text, y_train, ngrams, model, sent):
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer()),
        ('tfidf', TfidfTransformer())])

    sent_pipe = Pipeline([
        ('sent', sent),
        ('vect', DictVectorizer())])

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('sent_feats', sent_pipe)
        ])),
        ('clf', model)
    ])

    mod.fit(train_text, y_train) 
    return mod

### Hyperparameter Tuning
def mod_ngram_hypertune(train_text, y_train, model, kwargs):
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    mod = Pipeline([('ngrams', extractors.NGramExtractor(ngrams)),
                ('vect', DictVectorizer()),
                ('tfidf', TfidfTransformer()),
                ('model', model)])

    search = {
        "model__loss" : Categorical(['hinge', 'squared_hinge']),
        "model__C" : Real(1e-6, 1e+6, prior='log-uniform'),
        "model__tol" : Real(1e-7, 1e-1, prior='log-uniform')
    }


    opt = BayesSearchCV( #Do this for BayesSearch
        mod,
        [(search, 32)]
        )


    opt.fit(train_text, y_train)

    print("Best Params", opt.best_params_)
    print("val. score: %s" % opt.best_score_)
    return opt  

def mod_ngram_pos_hypertune(train_text, y_train, model, kwargs):
    """
    Models with ngrams and pos
    """
    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    print("Features: ", end='')
    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('pos', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())]) 

    mod = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pos_feats', pos_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        # ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))),
        ('model', model)
    ])

    search = {
        "model__loss" : Categorical(['hinge', 'log', 'modified_huber', 'squared_hinge', 'perceptron']),
        "model__penalty" : Categorical(["none", "l2", "l1", "elasticnet"]),
        "model__alpha" : Real(1e-7, 1e-1, prior='log-uniform'),
        "model__max_iter" : Integer(4, 2000),
        "model__tol" : Real(1e-7, 1e-1, prior='log-uniform')
    }


    opt = BayesSearchCV( #Do this for BayesSearch
        mod,
        [(search, 32)]
        )


    opt.fit(train_text, y_train)

    print("Best Params", opt.best_params_)
    print("val. score: %s" % opt.best_score_)
    return opt   



### Ensemble Classification

def mod_voting_classifier(train_text, y_train, models, kwargs):
    """
    models is a list of models
    """
    if not models is None:
        warn('Ignorning models')


    if 'ngrams' in kwargs:
        ngrams = kwargs['ngrams']
    else:
        raise ValueError('Must pass number of ngrams to model')

    if 'posgrams' in kwargs:
        posgrams = kwargs['posgrams']
    else:
        raise ValueError('Must pass number of posgrams to model')

    if 'detailed' in kwargs:
        detailed = kwargs['detailed']
    else:
        raise ValueError('Must specify whether POS tags are detailed')

    models = Models()

    print("Features: ", end='')

    count_pipe = Pipeline([
        ('ngrams', extractors.NGramExtractor(ngrams)),
        ('vect', DictVectorizer())])

    dep_pipe = Pipeline([
        ('dep', extractors.DEPExtractor()),
        ('vect', DictVectorizer())])

    pos_pipe = Pipeline([
        ('pos', extractors.POSExtractor(posgrams, detailed)),
        ('vect', DictVectorizer())]) 


    liwc_pipe = Pipeline([
        ('liwc', extractors.LIWCExtractor()),
        ('vect', DictVectorizer())])



    pipe_mlp_ngram_dep  = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('dep_feats', dep_pipe)
        ])),
        ('tfidf', TfidfTransformer()),
        # ('feature_selection', SelectFromModel(LinearSVC(C=1.5))),
        ('clf', models.get('mlp'))
    ])

    pipe_sgd_ngram_liwc = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('liwc_feats', liwc_pipe)
        ])),
        ('tfidf', TfidfTransformer()), # Doing a single TFIDF transform seems to be better than individual
        # ('feature_selection', SelectFromModel(LinearSVC(C=1.5))),
        ('clf', models.get('sgd'))
    ])


    #Try MLP with unigrams and liwc

    pipe_linear_svm_ngram_pos  = Pipeline([
        ('feats', FeatureUnion([
            ('count_feats', count_pipe),
            ('pos_feats', pos_pipe)
        ])),
        ('tfidf', TfidfTransformer()), #Note: Can experiment with other transform types
        # ('feature_selection', SelectFromModel(LinearSVC(C=1.5, random_state=rand_state))), 
        ('clf', models.get('linear_svm'))
    ])

    # pipe_sgd_dep = Pipeline([('dep', extractors.DEPExtractor()),
    #                 ('vect', DictVectorizer()),
    #                 ('tfidf', TfidfTransformer()),
    #                 ('clf', models.get('sgd'))])


    # pipe_mlp_pos = Pipeline([('dep', extractors.POSExtractor(posgrams, detailed)),
    #                 ('vect', DictVectorizer()),
    #                 ('tfidf', TfidfTransformer()),
    #                 ('feature_selection', SelectFromModel(LinearSVC(C=1.5))),
    #                 ('clf', models.get('mlp'))])

    eclf = VotingClassifier(estimators=[
        ('sgd_ngram', pipe_mlp_ngram_dep),
        ('linear_svm_ngram', pipe_sgd_ngram_liwc),
        ('mlp_ngram', pipe_linear_svm_ngram_pos)],
        voting='hard')

    eclf.fit(train_text, y_train)
    return eclf
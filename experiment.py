import settings
import sys
import os

from models import *
from baseline import *

def main():
    settings.init()
    # Try
    # linear_svm Unigrams + POS + PCFG [Feature Selection]
    # linear_svm Bigrams + POS + PCFG [Feature Selection]

    runTest(testBestModels, None, None)
    # runTest(testOttModels, "all", "ottAllPolarityModels")
    # runTest(testCrossTrainSVM, "all", "crossTrainTestsAllUsersCollapsedSVMCont2")

def runTest(test_func, polarity, outfile=None):

    if not outfile is None:
        outpath = os.path.join("output", outfile + ".txt")
        with open(outpath, 'w+') as out:
            sys.stdout = out
            test_func(polarity)
            sys.stdout = sys.__stdout__
    else:
        test_func(polarity)

def testCrossTrain(polarity):
    models = Models()
    
    print('################################ Ngrams ################################')
    crossTrainExperiment(mod_ngram, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1)
    crossTrainExperiment(mod_ngram, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2)
    crossTrainExperiment(mod_ngram, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=3)

    crossTrainExperiment(mod_ngram, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1)
    crossTrainExperiment(mod_ngram, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2)
    crossTrainExperiment(mod_ngram, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=3)

    print('################################ POS ################################')
    crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False)
    crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True)

    crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=2, detailed=False)
    crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=2, detailed=True)

    crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, posgrams='2+', detailed=False)
    crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, posgrams='2+', detailed=True)

    crossTrainExperiment(mod_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False)
    crossTrainExperiment(mod_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True)

    crossTrainExperiment(mod_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=2, detailed=False)
    crossTrainExperiment(mod_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=2, detailed=True)

    crossTrainExperiment(mod_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, posgrams='2+', detailed=False)
    crossTrainExperiment(mod_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, posgrams='2+', detailed=True)

    print('################################ DEP and LIWC (separate) ################################')
    crossTrainExperiment(mod_dep, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True)
    crossTrainExperiment(mod_liwc, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True)

    crossTrainExperiment(mod_dep, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True)
    crossTrainExperiment(mod_liwc, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True)

    print('################################ Bigrams + LIWC ################################')
    crossTrainExperiment(mod_ngram_liwc, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2)
    crossTrainExperiment(mod_ngram_liwc, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2)


    print('################################ Bigrams + POS ################################')
    crossTrainExperiment(mod_ngram_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=1, detailed=False)
    crossTrainExperiment(mod_ngram_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=1, detailed=True)

    crossTrainExperiment(mod_ngram_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=2, detailed=False)
    crossTrainExperiment(mod_ngram_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=2, detailed=True)

    crossTrainExperiment(mod_ngram_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams='2+', detailed=False)
    crossTrainExperiment(mod_ngram_pos, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams='2+', detailed=True)

    crossTrainExperiment(mod_ngram_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=1, detailed=False)
    crossTrainExperiment(mod_ngram_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=1, detailed=True)

    crossTrainExperiment(mod_ngram_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=2, detailed=False)
    crossTrainExperiment(mod_ngram_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=2, detailed=True)

    crossTrainExperiment(mod_ngram_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams='2+', detailed=False)
    crossTrainExperiment(mod_ngram_pos, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams='2+', detailed=True)

    print('################################ Syntax (POS + DEP) ################################')
    crossTrainExperiment(mod_syntax, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False)
    crossTrainExperiment(mod_syntax, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True)

    crossTrainExperiment(mod_syntax, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False)
    crossTrainExperiment(mod_syntax, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True)

    print('################################ Non-Ngram (POS + DEP + LIWC) ################################')
    crossTrainExperiment(mod_non_ngram, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False)
    crossTrainExperiment(mod_non_ngram, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True)

    crossTrainExperiment(mod_non_ngram, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False)
    crossTrainExperiment(mod_non_ngram, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True)


    print('################################ All (Ngrams + POS + DEP + LIWC) ################################')
    crossTrainExperiment(mod_all, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=1, detailed=False)
    crossTrainExperiment(mod_all, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=1, detailed=True)

    crossTrainExperiment(mod_all, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=1, detailed=False)
    crossTrainExperiment(mod_all, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=1, detailed=True)

    crossTrainExperiment(mod_all, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=2, detailed=False)
    crossTrainExperiment(mod_all, models.get('sgd'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=2, detailed=True)

    crossTrainExperiment(mod_all, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=2, detailed=False)
    crossTrainExperiment(mod_all, models.get('sgd_l1'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=2, detailed=True)

def testCrossTrainSVM(polarity):
    models = Models()
    
    # print('################################ Ngrams ################################')
    # crossTrainExperiment(mod_ngram, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1)
    # crossTrainExperiment(mod_ngram, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2)
    # crossTrainExperiment(mod_ngram, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=3)

    # print('################################ POS ################################')
    # crossTrainExperiment(mod_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False)
    # crossTrainExperiment(mod_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True)

    # crossTrainExperiment(mod_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=2, detailed=False)
    # crossTrainExperiment(mod_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=2, detailed=True)

    # crossTrainExperiment(mod_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams='2+', detailed=False)
    # crossTrainExperiment(mod_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams='2+', detailed=True)

    # print('################################ PCFG ################################')
    # crossTrainExperiment(mod_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, lexicalized=False, include_gp=False)
    # crossTrainExperiment(mod_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, lexicalized=True, include_gp=False)
    # crossTrainExperiment(mod_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, lexicalized=False, include_gp=True)
    # crossTrainExperiment(mod_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, lexicalized=True, include_gp=True)


    # print('################################ DEP and LIWC (separate) ################################')
    # crossTrainExperiment(mod_dep, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True)
    # crossTrainExperiment(mod_liwc, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True)


    # print('################################ Unigrams + PCFG ################################')
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity,  ngrams=1, collapse_users=True, lexicalized=False, include_gp=False)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity,  ngrams=1, collapse_users=True, lexicalized=True, include_gp=False)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity,  ngrams=1, collapse_users=True, lexicalized=False, include_gp=True)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity,  ngrams=1, collapse_users=True, lexicalized=True, include_gp=True)

    # print('################################ Bigrams + PCFG ################################')
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity,  ngrams=2, collapse_users=True, lexicalized=False, include_gp=False)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity,  ngrams=2, collapse_users=True, lexicalized=True, include_gp=False)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity,  ngrams=2, collapse_users=True, lexicalized=False, include_gp=True)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), train="open_domain", polarity=polarity,  ngrams=2, collapse_users=True, lexicalized=True, include_gp=True)

    # print('################################ Bigrams + LIWC ################################')
    # crossTrainExperiment(mod_ngram_liwc, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2)


    # print('################################ Bigrams + POS ################################')
    # crossTrainExperiment(mod_ngram_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=1, detailed=False)
    # crossTrainExperiment(mod_ngram_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=1, detailed=True)

    # crossTrainExperiment(mod_ngram_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=2, detailed=False)
    # crossTrainExperiment(mod_ngram_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams=2, detailed=True)

    # crossTrainExperiment(mod_ngram_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams='2+', detailed=False)
    # crossTrainExperiment(mod_ngram_pos, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=1, posgrams='2+', detailed=True)

    print('################################ Syntax (POS + DEP  + PCFG) ################################')
    crossTrainExperiment(mod_syntax, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False, lexicalized=False, include_gp=False)
    crossTrainExperiment(mod_syntax, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True, lexicalized=False, include_gp=False)

    crossTrainExperiment(mod_syntax, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False, lexicalized=True, include_gp=False)
    crossTrainExperiment(mod_syntax, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True, lexicalized=True, include_gp=False)

    crossTrainExperiment(mod_syntax, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False, lexicalized=False, include_gp=True)
    crossTrainExperiment(mod_syntax, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True, lexicalized=False, include_gp=True)

    crossTrainExperiment(mod_syntax, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False, lexicalized=True, include_gp=True)
    crossTrainExperiment(mod_syntax, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True, lexicalized=True, include_gp=True)


    print('################################ Non-Ngram (POS + DEP + LIWC) ################################')
    crossTrainExperiment(mod_non_ngram, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=False)
    crossTrainExperiment(mod_non_ngram, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, posgrams=1, detailed=True)


    print('################################ All (Ngrams + POS + DEP + LIWC + PCFG) ################################')
    crossTrainExperiment(mod_all, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=1, detailed=False, lexicalized=False, include_gp=False)
    crossTrainExperiment(mod_all, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=False)

    crossTrainExperiment(mod_all, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=2, detailed=False, lexicalized=False, include_gp=False)
    crossTrainExperiment(mod_all, models.get('linear_svm'), train="open_domain", polarity=polarity, collapse_users=True, ngrams=2, posgrams=2, detailed=True, lexicalized=False, include_gp=False)


def testOpenDomain(collapse_users, mod_name='sgd'):
    models = Models()

    print('################################ Ngrams ################################')
    openDomainFiveFold(mod_ngram, models.get(mod_name), ngrams=1, collapse_users=collapse_users)
    openDomainFiveFold(mod_ngram, models.get(mod_name), ngrams=2, collapse_users=collapse_users)
    openDomainFiveFold(mod_ngram, models.get(mod_name), ngrams=3, collapse_users=collapse_users)

    print('################################ POS ################################')
    openDomainFiveFold(mod_pos, models.get(mod_name), posgrams=1, detailed=False, collapse_users=collapse_users)
    openDomainFiveFold(mod_pos, models.get(mod_name), posgrams=1, detailed=True, collapse_users=collapse_users)
    openDomainFiveFold(mod_pos, models.get(mod_name), posgrams=2, detailed=False, collapse_users=collapse_users)
    openDomainFiveFold(mod_pos, models.get(mod_name), posgrams=2, detailed=True, collapse_users=collapse_users)
    openDomainFiveFold(mod_pos, models.get(mod_name), posgrams='2+', detailed=False, collapse_users=collapse_users)
    openDomainFiveFold(mod_pos, models.get(mod_name), posgrams='2+', detailed=True, collapse_users=collapse_users)

    print('################################ DEP and LIWC (separate) ################################')
    openDomainFiveFold(mod_dep, models.get(mod_name), collapse_users=collapse_users)
    openDomainFiveFold(mod_liwc, models.get(mod_name), collapse_users=collapse_users)

    print('################################ Ngrams + LIWC ################################')
    openDomainFiveFold(mod_ngram_liwc, models.get(mod_name), ngrams=1, collapse_users=collapse_users)
    openDomainFiveFold(mod_ngram_liwc, models.get(mod_name), ngrams=2, collapse_users=collapse_users)
    openDomainFiveFold(mod_ngram_liwc, models.get(mod_name), ngrams=3, collapse_users=collapse_users)

    print('################################ Bigrams + POS ################################')
    openDomainFiveFold(mod_ngram_pos, models.get(mod_name), ngrams=2, posgrams=1, detailed=False, collapse_users=collapse_users)
    openDomainFiveFold(mod_ngram_pos, models.get(mod_name), ngrams=2, posgrams=1, detailed=True, collapse_users=collapse_users)
    openDomainFiveFold(mod_ngram_pos, models.get(mod_name), ngrams=2, posgrams=2, detailed=False, collapse_users=collapse_users)
    openDomainFiveFold(mod_ngram_pos, models.get(mod_name), ngrams=2, posgrams=2, detailed=True, collapse_users=collapse_users)
    openDomainFiveFold(mod_ngram_pos, models.get(mod_name), ngrams=2, posgrams='2+', detailed=False, collapse_users=collapse_users)
    openDomainFiveFold(mod_ngram_pos, models.get(mod_name), ngrams=2, posgrams='2+', detailed=True, collapse_users=collapse_users)

    print('################################ Syntax (POS + DEP) ################################')
    openDomainFiveFold(mod_syntax, models.get(mod_name), posgrams=1, detailed=False, collapse_users=collapse_users)
    openDomainFiveFold(mod_syntax, models.get(mod_name), posgrams=1, detailed=True, collapse_users=collapse_users)
    openDomainFiveFold(mod_syntax, models.get(mod_name), posgrams='2+', detailed=False, collapse_users=collapse_users)
    openDomainFiveFold(mod_syntax, models.get(mod_name), posgrams='2+', detailed=True, collapse_users=collapse_users)

    print('################################ Non-Ngram (POS + DEP + LIWC) ################################')
    openDomainFiveFold(mod_non_ngram, models.get(mod_name), posgrams=1, detailed=True, collapse_users=collapse_users)
    openDomainFiveFold(mod_non_ngram, models.get(mod_name), posgrams=2, detailed=True, collapse_users=collapse_users)
    openDomainFiveFold(mod_non_ngram, models.get(mod_name), posgrams='2+', detailed=True, collapse_users=collapse_users)


    print('################################ All (Ngrams + POS + DEP + LIWC) ################################')
    openDomainFiveFold(mod_all, models.get(mod_name), ngrams=1, posgrams=1, detailed=False, collapse_users=collapse_users)
    openDomainFiveFold(mod_all, models.get(mod_name), ngrams=1, posgrams=1, detailed=True, collapse_users=collapse_users)
    openDomainFiveFold(mod_all, models.get(mod_name), ngrams=1, posgrams='2+', detailed=True, collapse_users=collapse_users)

    openDomainFiveFold(mod_all, models.get(mod_name), ngrams=2, posgrams=1, detailed=False, collapse_users=collapse_users)
    openDomainFiveFold(mod_all, models.get(mod_name), ngrams=2, posgrams=1, detailed=True, collapse_users=collapse_users)
    openDomainFiveFold(mod_all, models.get(mod_name), ngrams=2, posgrams='2+', detailed=True, collapse_users=collapse_users)


def testBestOtt(polarity):
    models = Models()

    # ottFiveFold(mod_ngram_pos, models.get('linear_svm'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)   

    # ottFiveFold(mod_ngram_feature_select, models.get('linear_svm'), polarity=polarity, ngrams=2) 

    # ottFiveFold(mod_ngram_pos_feature_select, models.get('linear_svm'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)

    # ottFiveFold(mod_ngram_dep_feature_select, models.get('linear_svm'), polarity=polarity, ngrams=2)


    ottFiveFold(mod_ngram_pos, models.get('sgd'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)   

    ottFiveFold(mod_ngram_feature_select, models.get('sgd'), polarity=polarity, ngrams=2) 

    ottFiveFold(mod_ngram_pos_feature_select, models.get('sgd'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)

    ottFiveFold(mod_ngram_dep_feature_select, models.get('sgd'), polarity=polarity, ngrams=2)


def testOtt2013(polarity):
    if not polarity is None:
        warn("Ignoring polarity for ott2013Tests")

    models = Models()

    ottFiveFold(mod_ngram, models.get('linear_svm'), polarity='positive', ngrams=2)

    ottFiveFold(mod_ngram, models.get('linear_svm'), polarity='negative', ngrams=2)

    ottFiveFold(mod_ngram, models.get('linear_svm'), polarity='all', ngrams=2)


def testOtt2011(polarity):
    models = Models()

    # # Genre Identification
    # ottFiveFold(mod_pos, models.get('svm'), polarity=polarity, posgrams=1, detailed=True)

    # # # Psycholinguistic Deception Detection
    # ottFiveFold(mod_liwc, models.get('svm'), polarity=polarity)

    # # # Text Catergorization
    # ottFiveFold(mod_ngram, models.get('svm'), polarity=polarity, ngrams=1)
    # ottFiveFold(mod_ngram, models.get('svm'), polarity=polarity, ngrams=2)
    # ottFiveFold(mod_ngram_liwc, models.get('svm'), polarity=polarity, ngrams=2)
    # ottFiveFold(mod_ngram, models.get('svm'), polarity=polarity, ngrams=3)
    # ottFiveFold(mod_ngram, models.get('nb'), polarity=polarity, ngrams=1)
    # ottFiveFold(mod_ngram, models.get('nb'), polarity=polarity, ngrams=2)
    # ottFiveFold(mod_ngram, models.get('nb'), polarity=polarity, ngrams=3)


    # Genre Identification
    ottFiveFold(mod_pos, models.get('linear_svm'), polarity=polarity, posgrams=1, detailed=True)

    # # Psycholinguistic Deception Detection
    ottFiveFold(mod_liwc, models.get('linear_svm'), polarity=polarity)

    # # Text Catergorization
    ottFiveFold(mod_ngram, models.get('linear_svm'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram, models.get('linear_svm'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_liwc, models.get('linear_svm'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram, models.get('linear_svm'), polarity=polarity, ngrams=3)
    ottFiveFold(mod_ngram, models.get('nb'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram, models.get('nb'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram, models.get('nb'), polarity=polarity, ngrams=3)


def testOttModels(polarity):
    models = Models()

    print('################################ Unigrams ################################')
    ottFiveFold(mod_ngram, models.get('svm'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram, models.get('nb'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram, models.get('random_forest'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram, models.get('logit'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram, models.get('mlp'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram, models.get('sgd'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram, models.get('linear_svm'), polarity=polarity, ngrams=1)

    print('################################ Bigrams ################################')
    ottFiveFold(mod_ngram, models.get('svm'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram, models.get('nb'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram, models.get('random_forest'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram, models.get('logit'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram, models.get('mlp'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram, models.get('sgd'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram, models.get('linear_svm'), polarity=polarity, ngrams=2)

    print('################################ Trigrams ################################')
    ottFiveFold(mod_ngram, models.get('svm'), polarity=polarity, ngrams=3)
    ottFiveFold(mod_ngram, models.get('nb'), polarity=polarity, ngrams=3)
    ottFiveFold(mod_ngram, models.get('random_forest'), polarity=polarity, ngrams=3)
    ottFiveFold(mod_ngram, models.get('logit'), polarity=polarity, ngrams=3)
    ottFiveFold(mod_ngram, models.get('mlp'), polarity=polarity, ngrams=3)
    ottFiveFold(mod_ngram, models.get('sgd'), polarity=polarity, ngrams=3)
    ottFiveFold(mod_ngram, models.get('linear_svm'), polarity=polarity, ngrams=3)

    print('################################ Unigrams + LIWC ################################')
    ottFiveFold(mod_ngram_liwc, models.get('svm'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_liwc, models.get('nb'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_liwc, models.get('random_forest'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_liwc, models.get('logit'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_liwc, models.get('mlp'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_liwc, models.get('sgd'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_liwc, models.get('linear_svm'), polarity=polarity, ngrams=1)

    print('################################ Bigrams + LIWC ################################')
    ottFiveFold(mod_ngram_liwc, models.get('svm'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_liwc, models.get('nb'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_liwc, models.get('random_forest'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_liwc, models.get('logit'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_liwc, models.get('mlp'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_liwc, models.get('sgd'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_liwc, models.get('linear_svm'), polarity=polarity, ngrams=2)

    print('################################ Unigrams + POS ################################')
    ottFiveFold(mod_ngram_pos, models.get('svm'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('nb'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('random_forest'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('logit'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('mlp'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('sgd'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('linear_svm'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)

    print('################################ Bigrams + POS ################################')
    ottFiveFold(mod_ngram_pos, models.get('svm'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('nb'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('random_forest'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('logit'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('mlp'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('sgd'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('linear_svm'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)

    print('################################ Unigrams + BIPOS ################################')
    ottFiveFold(mod_ngram_pos, models.get('svm'), polarity=polarity, ngrams=1, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('nb'), polarity=polarity, ngrams=1, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('random_forest'), polarity=polarity, ngrams=1, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('logit'), polarity=polarity, ngrams=1, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('mlp'), polarity=polarity, ngrams=1, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('sgd'), polarity=polarity, ngrams=1, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('linear_svm'), polarity=polarity, ngrams=1, posgrams=2, detailed=False)

    print('################################ Unigrams + BIPOS+ ################################')
    ottFiveFold(mod_ngram_pos, models.get('svm'), polarity=polarity, ngrams=1, posgrams='2+', detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('nb'), polarity=polarity, ngrams=1, posgrams='2+', detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('random_forest'), polarity=polarity, ngrams=1, posgrams='2+', detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('logit'), polarity=polarity, ngrams=1, posgrams='2+', detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('mlp'), polarity=polarity, ngrams=1, posgrams='2+', detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('sgd'), polarity=polarity, ngrams=1, posgrams='2+', detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('linear_svm'), polarity=polarity, ngrams=1, posgrams='2+', detailed=False)
   
    print('################################ Bigrams + BIPOS ################################')
    ottFiveFold(mod_ngram_pos, models.get('svm'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('nb'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('random_forest'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('logit'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('mlp'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('sgd'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_ngram_pos, models.get('linear_svm'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)

    print('################################ Bigrams + BIPOS+ ################################')
    ottFiveFold(mod_ngram_pos, models.get('svm'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('nb'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('random_forest'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('logit'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('mlp'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('sgd'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_pos, models.get('linear_svm'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)

    print('################################ Unigrams + DEP ################################')
    ottFiveFold(mod_ngram_dep, models.get('svm'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep, models.get('nb'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep, models.get('random_forest'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep, models.get('logit'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep, models.get('mlp'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep, models.get('sgd'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep, models.get('linear_svm'), polarity=polarity, ngrams=1)

    print('################################ Bigrams + DEP ################################')
    ottFiveFold(mod_ngram_dep, models.get('svm'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_dep, models.get('nb'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_dep, models.get('random_forest'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_dep, models.get('logit'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_dep, models.get('mlp'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_dep, models.get('sgd'), polarity=polarity, ngrams=2)
    ottFiveFold(mod_ngram_dep, models.get('linear_svm'), polarity=polarity, ngrams=2)


    print('################################ Bigrams + BIPOS+ + DEP ################################')
    ottFiveFold(mod_ngram_dep_pos, models.get('svm'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('nb'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('random_forest'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('logit'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('mlp'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('sgd'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('linear_svm'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)

    print('################################ Unigrams + DEP + POS ################################')
    ottFiveFold(mod_ngram_dep_pos, models.get('svm'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('nb'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('random_forest'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('logit'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('mlp'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('sgd'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_ngram_dep_pos, models.get('linear_svm'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)

    print('################################ Unigrams + DEP + LIWC ################################')
    ottFiveFold(mod_ngram_dep_liwc, models.get('svm'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep_liwc, models.get('nb'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep_liwc, models.get('random_forest'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep_liwc, models.get('logit'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep_liwc, models.get('mlp'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep_liwc, models.get('sgd'), polarity=polarity, ngrams=1)
    ottFiveFold(mod_ngram_dep_liwc, models.get('linear_svm'), polarity=polarity, ngrams=1)

    print('################################ Unigrams + POS + DEP + LIWC ################################')
    ottFiveFold(mod_all, models.get('svm'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('nb'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('random_forest'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('logit'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('mlp'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('sgd'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('linear_svm'), polarity=polarity, ngrams=1, posgrams=1, detailed=True)

    print('################################ Bigrams + POS + DEP + LIWC ################################')
    ottFiveFold(mod_all, models.get('svm'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('nb'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('random_forest'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('logit'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('mlp'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('sgd'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)
    ottFiveFold(mod_all, models.get('linear_svm'), polarity=polarity, ngrams=2, posgrams=1, detailed=True)

    print('################################ Bigrams + BIPOS + DEP + LIWC ################################')
    ottFiveFold(mod_all, models.get('svm'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_all, models.get('nb'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_all, models.get('random_forest'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_all, models.get('logit'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_all, models.get('mlp'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_all, models.get('sgd'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)
    ottFiveFold(mod_all, models.get('linear_svm'), polarity=polarity, ngrams=2, posgrams=2, detailed=False)

    print('################################ Bigrams + BIPOS+ + DEP + LIWC ################################')
    ottFiveFold(mod_all, models.get('svm'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_all, models.get('nb'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_all, models.get('random_forest'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_all, models.get('logit'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_all, models.get('mlp'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_all, models.get('sgd'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)
    ottFiveFold(mod_all, models.get('linear_svm'), polarity=polarity, ngrams=2, posgrams='2+', detailed=True)


def testOpenDomainModels(polarity):
    models = Models()

    print('################################ Unigrams ################################')
    openDomainFiveFold(mod_ngram, models.get('svm'), ngrams=1)
    openDomainFiveFold(mod_ngram, models.get('nb'), ngrams=1)
    openDomainFiveFold(mod_ngram, models.get('random_forest'), ngrams=1)
    openDomainFiveFold(mod_ngram, models.get('logit'), ngrams=1)
    openDomainFiveFold(mod_ngram, models.get('mlp'), ngrams=1)
    openDomainFiveFold(mod_ngram, models.get('sgd'), ngrams=1)
    openDomainFiveFold(mod_ngram, models.get('linear_svm'), ngrams=1)

    print('################################ Bigrams ################################')
    openDomainFiveFold(mod_ngram, models.get('svm'), ngrams=2)
    openDomainFiveFold(mod_ngram, models.get('nb'), ngrams=2)
    openDomainFiveFold(mod_ngram, models.get('random_forest'), ngrams=2)
    openDomainFiveFold(mod_ngram, models.get('logit'), ngrams=2)
    openDomainFiveFold(mod_ngram, models.get('mlp'), ngrams=2)
    openDomainFiveFold(mod_ngram, models.get('sgd'), ngrams=2)
    openDomainFiveFold(mod_ngram, models.get('linear_svm'), ngrams=2)

    print('################################ Trigrams ################################')
    openDomainFiveFold(mod_ngram, models.get('svm'), ngrams=3)
    openDomainFiveFold(mod_ngram, models.get('nb'), ngrams=3)
    openDomainFiveFold(mod_ngram, models.get('random_forest'), ngrams=3)
    openDomainFiveFold(mod_ngram, models.get('logit'), ngrams=3)
    openDomainFiveFold(mod_ngram, models.get('mlp'), ngrams=3)
    openDomainFiveFold(mod_ngram, models.get('sgd'), ngrams=3)
    openDomainFiveFold(mod_ngram, models.get('linear_svm'), ngrams=3)

    print('################################ Unigrams + LIWC ################################')
    openDomainFiveFold(mod_ngram_liwc, models.get('svm'), ngrams=1)
    openDomainFiveFold(mod_ngram_liwc, models.get('nb'), ngrams=1)
    openDomainFiveFold(mod_ngram_liwc, models.get('random_forest'), ngrams=1)
    openDomainFiveFold(mod_ngram_liwc, models.get('logit'), ngrams=1)
    openDomainFiveFold(mod_ngram_liwc, models.get('mlp'), ngrams=1)
    openDomainFiveFold(mod_ngram_liwc, models.get('sgd'), ngrams=1)
    openDomainFiveFold(mod_ngram_liwc, models.get('linear_svm'), ngrams=1)

    print('################################ Bigrams + LIWC ################################')
    openDomainFiveFold(mod_ngram_liwc, models.get('svm'), ngrams=2)
    openDomainFiveFold(mod_ngram_liwc, models.get('nb'), ngrams=2)
    openDomainFiveFold(mod_ngram_liwc, models.get('random_forest'), ngrams=2)
    openDomainFiveFold(mod_ngram_liwc, models.get('logit'), ngrams=2)
    openDomainFiveFold(mod_ngram_liwc, models.get('mlp'), ngrams=2)
    openDomainFiveFold(mod_ngram_liwc, models.get('sgd'), ngrams=2)
    openDomainFiveFold(mod_ngram_liwc, models.get('linear_svm'), ngrams=2)

    print('################################ Unigrams + POS ################################')
    openDomainFiveFold(mod_ngram_pos, models.get('svm'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('nb'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('random_forest'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('logit'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('mlp'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('sgd'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('linear_svm'), ngrams=1, posgrams=1, detailed=True)

    print('################################ Bigrams + POS ################################')
    openDomainFiveFold(mod_ngram_pos, models.get('svm'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('nb'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('random_forest'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('logit'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('mlp'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('sgd'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('linear_svm'), ngrams=2, posgrams=1, detailed=True)

    print('################################ Unigrams + BIPOS ################################')
    openDomainFiveFold(mod_ngram_pos, models.get('svm'), ngrams=1, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('nb'), ngrams=1, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('random_forest'), ngrams=1, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('logit'), ngrams=1, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('mlp'), ngrams=1, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('sgd'), ngrams=1, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('linear_svm'), ngrams=1, posgrams=2, detailed=False)

    print('################################ Unigrams + BIPOS+ ################################')
    openDomainFiveFold(mod_ngram_pos, models.get('svm'), ngrams=1, posgrams='2+', detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('nb'), ngrams=1, posgrams='2+', detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('random_forest'), ngrams=1, posgrams='2+', detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('logit'), ngrams=1, posgrams='2+', detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('mlp'), ngrams=1, posgrams='2+', detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('sgd'), ngrams=1, posgrams='2+', detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('linear_svm'), ngrams=1, posgrams='2+', detailed=False)
   
    print('################################ Bigrams + BIPOS ################################')
    openDomainFiveFold(mod_ngram_pos, models.get('svm'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('nb'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('random_forest'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('logit'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('mlp'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('sgd'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_ngram_pos, models.get('linear_svm'), ngrams=2, posgrams=2, detailed=False)

    print('################################ Bigrams + BIPOS+ ################################')
    openDomainFiveFold(mod_ngram_pos, models.get('svm'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('nb'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('random_forest'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('logit'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('mlp'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('sgd'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_pos, models.get('linear_svm'), ngrams=2, posgrams='2+', detailed=True)

    print('################################ Unigrams + DEP ################################')
    openDomainFiveFold(mod_ngram_dep, models.get('svm'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep, models.get('nb'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep, models.get('random_forest'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep, models.get('logit'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep, models.get('mlp'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep, models.get('sgd'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep, models.get('linear_svm'), ngrams=1)

    print('################################ Bigrams + DEP ################################')
    openDomainFiveFold(mod_ngram_dep, models.get('svm'), ngrams=2)
    openDomainFiveFold(mod_ngram_dep, models.get('nb'), ngrams=2)
    openDomainFiveFold(mod_ngram_dep, models.get('random_forest'), ngrams=2)
    openDomainFiveFold(mod_ngram_dep, models.get('logit'), ngrams=2)
    openDomainFiveFold(mod_ngram_dep, models.get('mlp'), ngrams=2)
    openDomainFiveFold(mod_ngram_dep, models.get('sgd'), ngrams=2)
    openDomainFiveFold(mod_ngram_dep, models.get('linear_svm'), ngrams=2)


    print('################################ Bigrams + BIPOS+ + DEP ################################')
    openDomainFiveFold(mod_ngram_dep_pos, models.get('svm'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('nb'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('random_forest'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('logit'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('mlp'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('sgd'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('linear_svm'), ngrams=2, posgrams='2+', detailed=True)

    print('################################ Unigrams + DEP + POS ################################')
    openDomainFiveFold(mod_ngram_dep_pos, models.get('svm'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('nb'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('random_forest'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('logit'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('mlp'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('sgd'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_ngram_dep_pos, models.get('linear_svm'), ngrams=1, posgrams=1, detailed=True)

    print('################################ Unigrams + DEP + LIWC ################################')
    openDomainFiveFold(mod_ngram_dep_liwc, models.get('svm'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep_liwc, models.get('nb'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep_liwc, models.get('random_forest'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep_liwc, models.get('logit'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep_liwc, models.get('mlp'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep_liwc, models.get('sgd'), ngrams=1)
    openDomainFiveFold(mod_ngram_dep_liwc, models.get('linear_svm'), ngrams=1)

    print('################################ Unigrams + POS + DEP + LIWC ################################')
    openDomainFiveFold(mod_all, models.get('svm'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('nb'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('random_forest'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('logit'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('mlp'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('sgd'), ngrams=1, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('linear_svm'), ngrams=1, posgrams=1, detailed=True)

    print('################################ Bigrams + POS + DEP + LIWC ################################')
    openDomainFiveFold(mod_all, models.get('svm'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('nb'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('random_forest'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('logit'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('mlp'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('sgd'), ngrams=2, posgrams=1, detailed=True)
    openDomainFiveFold(mod_all, models.get('linear_svm'), ngrams=2, posgrams=1, detailed=True)

    print('################################ Bigrams + BIPOS + DEP + POS Not Detailed ################################')
    openDomainFiveFold(mod_all, models.get('svm'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_all, models.get('nb'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_all, models.get('random_forest'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_all, models.get('logit'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_all, models.get('mlp'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_all, models.get('sgd'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_all, models.get('linear_svm'), ngrams=2, posgrams=2, detailed=False)

    print('################################ Bigrams + BIPOS+ + DEP + POS Detailed ################################')
    openDomainFiveFold(mod_all, models.get('svm'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_all, models.get('nb'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_all, models.get('random_forest'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_all, models.get('logit'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_all, models.get('mlp'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_all, models.get('sgd'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_all, models.get('linear_svm'), ngrams=2, posgrams='2+', detailed=True)

def testOpenDomainFeatureSelect(polarity):
    if not polarity is None:
        warn("Ignoring polarity for testOpenDomainFeatureSelect")

    models = Models()

    print('################################ Bigrams ################################')
    openDomainFiveFold(mod_ngram_feature_select, models.get('mlp'), ngrams=2)
    openDomainFiveFold(mod_ngram_feature_select, models.get('sgd'), ngrams=2)
    openDomainFiveFold(mod_ngram_feature_select, models.get('linear_svm'), ngrams=2)
    
    print('################################ Bigrams + BIPOS+ ################################')    
    openDomainFiveFold(mod_ngram_pos_feature_select, models.get('mlp'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_pos_feature_select, models.get('sgd'), ngrams=2, posgrams='2+', detailed=True)
    openDomainFiveFold(mod_ngram_pos_feature_select, models.get('linear_svm'), ngrams=2, posgrams='2+', detailed=True)

    print('################################ Bigrams + DEP ################################')
    openDomainFiveFold(mod_ngram_dep_feature_select, models.get('mlp'), ngrams=2)
    openDomainFiveFold(mod_ngram_dep_feature_select, models.get('sgd'), ngrams=2)
    openDomainFiveFold(mod_ngram_dep_feature_select, models.get('linear_svm'), ngrams=2)

    print('################################ Bigrams + BIPOS + DEP + LIWC Not Detailed ################################')
    openDomainFiveFold(mod_all_feature_select, models.get('mlp'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_all_feature_select, models.get('sgd'), ngrams=2, posgrams=2, detailed=False)
    openDomainFiveFold(mod_all_feature_select, models.get('linear_svm'), ngrams=2, posgrams=2, detailed=False)

def testOttPCFG(polarity):

    # print('############################################################################')
    # print('################################ NB ################################')
    # testOttPCFGModel(polarity, "nb")
    
    print('############################################################################')
    print('################################ MLP ################################')
    testOttPCFGModel(polarity, "mlp")

    print('############################################################################')
    print('################################ SGD ################################')
    testOttPCFGModel(polarity, "sgd")

    print('############################################################################')
    print('################################ linear_svm ################################')
    testOttPCFGModel(polarity, "linear_svm")

def testOttPCFGModel(polarity, model_name):
    models = Models()

    print('################################ PCFG ################################')
    ottFiveFold(mod_pcfg, models.get(model_name), polarity=polarity, lexicalized=False, include_gp=False)
    ottFiveFold(mod_pcfg, models.get(model_name), polarity=polarity, lexicalized=True, include_gp=False)
    ottFiveFold(mod_pcfg, models.get(model_name), polarity=polarity, lexicalized=False, include_gp=True)
    ottFiveFold(mod_pcfg, models.get(model_name), polarity=polarity, lexicalized=True, include_gp=True)

    print('################################ Unigrams + PCFG ################################')
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=False, include_gp=False)
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=True, include_gp=False)
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=False, include_gp=True)
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=True, include_gp=True)

    print('################################ Bigrams + PCFG ################################')
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=False, include_gp=False)
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=True, include_gp=False)
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=False, include_gp=True)
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=True, include_gp=True)

    print('################################ Trigrams + PCFG ################################')
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=3, lexicalized=False, include_gp=False)
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=3, lexicalized=True, include_gp=False)
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=3, lexicalized=False, include_gp=True)
    ottFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=3, lexicalized=True, include_gp=True)

    print('################################ PCFG + DEP ################################')
    ottFiveFold(mod_dep_pcfg, models.get(model_name), polarity=polarity, lexicalized=False, include_gp=False)
    ottFiveFold(mod_dep_pcfg, models.get(model_name), polarity=polarity, lexicalized=True, include_gp=False)
    ottFiveFold(mod_dep_pcfg, models.get(model_name), polarity=polarity, lexicalized=False, include_gp=True)
    ottFiveFold(mod_dep_pcfg, models.get(model_name), polarity=polarity, lexicalized=True, include_gp=True)

    print('################################ Unigrams + PCFG + DEP ################################')
    ottFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=False, include_gp=False)
    ottFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=True, include_gp=False)
    ottFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=False, include_gp=True)
    ottFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=True, include_gp=True)

    print('################################ Bigrams + PCFG + DEP ################################')
    ottFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=False, include_gp=False)
    ottFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=True, include_gp=False)
    ottFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=False, include_gp=True)
    ottFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=True, include_gp=True)

    print('################################ PCFG + POS ################################')
    ottFiveFold(mod_pos_pcfg, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    ottFiveFold(mod_pos_pcfg, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    ottFiveFold(mod_pos_pcfg, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    ottFiveFold(mod_pos_pcfg, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    print('################################ PCFG + POS + DEP ################################')
    ottFiveFold(mod_syntax, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    ottFiveFold(mod_syntax, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    ottFiveFold(mod_syntax, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    ottFiveFold(mod_syntax, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    print('################################ Unigrams + POS + PCFG ################################')
    ottFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    ottFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    ottFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    ottFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    print('################################ Bigrams + POS + PCFG ################################')
    ottFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    ottFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    ottFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    ottFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    print('################################ Unigrams + POS + PCFG + DEP + LIWC ################################')
    ottFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    ottFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    ottFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    ottFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    print('################################ Bigrams + POS + PCFG + DEP + LIWC ################################')
    ottFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    ottFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    ottFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    ottFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    print('################################ Unigrams + POS + PCFG + DEP + LIWC [Feature Select] ################################')
    ottFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    ottFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    ottFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    ottFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    print('################################ Bigrams + POS + PCFG + DEP + LIWC [Feature Select]  ################################')
    ottFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    ottFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    ottFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    ottFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=True)


def testOpenDomainPCFG(polarity):

    # print('############################################################################')
    # print('################################ NB ################################')
    # testOpenDomainPCFGModel(polarity, "nb")
    
    # print('############################################################################')
    # print('################################ MLP ################################')
    # testOpenDomainPCFGModel(polarity, "mlp")

    print('############################################################################')
    print('################################ linear_svm ################################')
    testOpenDomainPCFGModel(polarity, "linear_svm")

    print('############################################################################')
    print('################################ SGD ################################')
    testOpenDomainPCFGModel(polarity, "sgd")

def testOpenDomainPCFGModel(polarity, model_name):
    models = Models()

    print('################################ PCFG ################################')
    openDomainFiveFold(mod_pcfg, models.get(model_name), polarity=polarity, lexicalized=False, include_gp=False)
    openDomainFiveFold(mod_pcfg, models.get(model_name), polarity=polarity, lexicalized=True, include_gp=False)
    openDomainFiveFold(mod_pcfg, models.get(model_name), polarity=polarity, lexicalized=False, include_gp=True)
    openDomainFiveFold(mod_pcfg, models.get(model_name), polarity=polarity, lexicalized=True, include_gp=True)

    print('################################ Unigrams + PCFG ################################')
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=False, include_gp=False)
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=True, include_gp=False)
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=False, include_gp=True)
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=True, include_gp=True)

    print('################################ Bigrams + PCFG ################################')
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=False, include_gp=False)
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=True, include_gp=False)
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=False, include_gp=True)
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=True, include_gp=True)

    print('################################ Trigrams + PCFG ################################')
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=3, lexicalized=False, include_gp=False)
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=3, lexicalized=True, include_gp=False)
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=3, lexicalized=False, include_gp=True)
    openDomainFiveFold(mod_ngram_pcfg, models.get(model_name), polarity=polarity, ngrams=3, lexicalized=True, include_gp=True)

    print('################################ PCFG + DEP ################################')
    openDomainFiveFold(mod_dep_pcfg, models.get(model_name), polarity=polarity, lexicalized=False, include_gp=False)
    openDomainFiveFold(mod_dep_pcfg, models.get(model_name), polarity=polarity, lexicalized=True, include_gp=False)
    openDomainFiveFold(mod_dep_pcfg, models.get(model_name), polarity=polarity, lexicalized=False, include_gp=True)
    openDomainFiveFold(mod_dep_pcfg, models.get(model_name), polarity=polarity, lexicalized=True, include_gp=True)

    print('################################ Unigrams + PCFG + DEP ################################')
    openDomainFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=False, include_gp=False)
    openDomainFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=True, include_gp=False)
    openDomainFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=False, include_gp=True)
    openDomainFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=1, lexicalized=True, include_gp=True)

    print('################################ Bigrams + PCFG + DEP ################################')
    openDomainFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=False, include_gp=False)
    openDomainFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=True, include_gp=False)
    openDomainFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=False, include_gp=True)
    openDomainFiveFold(mod_ngram_dep_pcfg, models.get(model_name), polarity=polarity, ngrams=2, lexicalized=True, include_gp=True)

    print('################################ PCFG + POS ################################')
    openDomainFiveFold(mod_pos_pcfg, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    openDomainFiveFold(mod_pos_pcfg, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    openDomainFiveFold(mod_pos_pcfg, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    openDomainFiveFold(mod_pos_pcfg, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    # print('################################ PCFG + POS + DEP ################################')
    # openDomainFiveFold(mod_syntax, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    # openDomainFiveFold(mod_syntax, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    # openDomainFiveFold(mod_syntax, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    # openDomainFiveFold(mod_syntax, models.get(model_name), polarity=polarity, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    print('################################ Unigrams + POS + PCFG ################################')
    openDomainFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    openDomainFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    openDomainFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    openDomainFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    print('################################ Bigrams + POS + PCFG ################################')
    openDomainFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    openDomainFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    openDomainFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    openDomainFiveFold(mod_ngram_pos_pcfg, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    # print('################################ Unigrams + POS + PCFG + DEP + LIWC ################################')
    # openDomainFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    # openDomainFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    # openDomainFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    # openDomainFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    # print('################################ Bigrams + POS + PCFG + DEP + LIWC ################################')
    # openDomainFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    # openDomainFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    # openDomainFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    # openDomainFiveFold(mod_all, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    # print('################################ Unigrams + POS + PCFG + DEP + LIWC [Feature Select] ################################')
    # openDomainFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    # openDomainFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    # openDomainFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    # openDomainFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=1, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

    # print('################################ Bigrams + POS + PCFG + DEP + LIWC [Feature Select]  ################################')
    # openDomainFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    # openDomainFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=False)
    # openDomainFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    # openDomainFiveFold(mod_all_feature_select, models.get(model_name), polarity=polarity, ngrams=2, posgrams=1, detailed=True, lexicalized=True, include_gp=True)

def  testBestModels(polarity):
    if not polarity is None:
        warn("Ignoring polarity for best model test")

    models = Models()
    print("### Ott positive with SVM_linear: Bigrams + unlexicalized PCFG (r)")
    ottFiveFold(mod_ngram_pcfg, models.get('linear_svm'), polarity="positive", ngrams=2, lexicalized=False, include_gp=False)

    print("### Ott negative with SVM_linear: Bigrams + unlexicalized PCFG (r)")
    ottFiveFold(mod_ngram_pcfg, models.get('linear_svm'), polarity="negative", ngrams=2, lexicalized=False, include_gp=False)

    print("### Ott all with SVM_linear: Bigrams + lexicalized PCFG (r*)")
    ottFiveFold(mod_ngram_pcfg, models.get('linear_svm'), polarity="all", ngrams=2, lexicalized=True, include_gp=False)

    print("### Open Domain with SVM_linear: Bigrams + DEP")
    openDomainFiveFold(mod_ngram_dep, models.get('linear_svm'), ngrams=2)

    print('### Cross Train with SGD: BIPOS+')
    crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", polarity="all", collapse_users=True, posgrams='2+', detailed=True)


if __name__ == '__main__':
    main()

#### Miscellaneous Experiments
    # runTest(testCrossTrain, "positive", "crossTrainTestsPositivePolarityUsersCollapsed") #Collapse users
    # runTest(testCrossTrain, "negative", "crossTrainTestsNegativePolarityUsersCollapsed") #Collapse users
    # runTest(testOpenDomainModels, None, "openDomainModelsUsersCollapsed")
    # runTest(testOpenDomainFeatureSelect, None, "openDomainFeatureSelectUsersCollapsed")

    # runTest(testOtt2013, None, "ott2013LinearSVM1")
    # runTest(testOtt2013, None, None)


    # runTest(lambda polarity: ottExperiment(mod_ngram, models.get('linear_svm'), polarity=polarity, ngrams=2), polarity="all", "ottVerboseExperiment")

    # models = Models()
    # ottFiveFold(mod_ngram_pcfg, models.get('linear_svm'), polarity="all", collapse_users=True, ngrams=2, lexicalized=True, include_gp=False)


    # ottExperiment(mod_ngram, models.get('linear_svm'), polarity="negative", collapse_users=True, ngrams=2)

    # ottFiveFold(mod_ngram_pcfg, models.get('linear_svm'), polarity="negative", ngrams=2, lexicalized=False, include_gp=False)
    # ottFiveFold(mod_ngram_pcfg, models.get('linear_svm'), polarity="negative", ngrams=2, lexicalized=True, include_gp=False)
    # ottFiveFold(mod_ngram_pcfg, models.get('linear_svm'), polarity="negative", ngrams=2, lexicalized=False, include_gp=True)
    # ottFiveFold(mod_ngram_pcfg, models.get('linear_svm'), polarity="negative", ngrams=2, lexicalized=True, include_gp=True)

    # ottFiveFold(mod_ngram_pcfg, models.get('sgd'), polarity="negative", ngrams=2, lexicalized=False, include_gp=False)
    # ottFiveFold(mod_ngram_pcfg, models.get('sgd'), polarity="negative", ngrams=2, lexicalized=True, include_gp=False)
    # ottFiveFold(mod_ngram_pcfg, models.get('sgd'), polarity="negative", ngrams=2, lexicalized=False, include_gp=True)
    # ottFiveFold(mod_ngram_pcfg, models.get('sgd'), polarity="negative", ngrams=2, lexicalized=True, include_gp=True)


    # openDomainExperiment(mod_ngram, models.get('linear_svm'), collapse_users=True, ngrams=2)
    # openDomainFiveFold(mod_ngram_pcfg, models.get('linear_svm'), collapse_users=True, ngrams=2, lexicalized=True, include_gp=False)
    # openDomainExperiment(mod_ngram_dep, models.get('linear_svm'), collapse_users=True, ngrams=2, verbose=True)
    # openDomainExperiment(mod_ngram_dep_pcfg, models.get('linear_svm'), collapse_users=True, ngrams=2, lexicalized=False, include_gp=False)


    # runTest(testOttPCFG, "all", "ottAllPolarityPCFG1")

    # runTest(testOpenDomainPCFG, None, "openDomainCollapsedUsersPCFG")

    # crossTrainExperiment(mod_ngram, models.get('linear_svm'), polarity='all', train='open_domain', ngrams=2)
    # crossTrainExperiment(mod_dep, models.get('linear_svm'), polarity='all', train='open_domain')
    # crossTrainExperiment(mod_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', lexicalized=False, include_gp=False)
    # crossTrainExperiment(mod_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', lexicalized=True, include_gp=False)
    # crossTrainExperiment(mod_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', lexicalized=False, include_gp=True)
    # crossTrainExperiment(mod_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', lexicalized=True, include_gp=True)

    # crossTrainExperiment(mod_pcfg, models.get('sgd'), polarity='all', train='open_domain', ngrams=2, lexicalized=False, include_gp=False)

    # crossTrainExperiment(mod_ngram_pcfg, models.get('sgd'), polarity='all', train='open_domain', ngrams=2, lexicalized=False, include_gp=False)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('sgd'), polarity='all', train='open_domain', ngrams=2, lexicalized=True, include_gp=False)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('sgd'), polarity='all', train='open_domain', ngrams=2, lexicalized=False, include_gp=True)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('sgd'), polarity='all', train='open_domain', ngrams=2, lexicalized=True, include_gp=True)

    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', ngrams=2, lexicalized=False, include_gp=False)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', ngrams=2, lexicalized=True, include_gp=False)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', ngrams=2, lexicalized=False, include_gp=True)
    # crossTrainExperiment(mod_ngram_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', ngrams=2, lexicalized=True, include_gp=True)

    # crossTrainExperiment(mod_dep_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', lexicalized=False, include_gp=False)
    # crossTrainExperiment(mod_pos_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    # crossTrainExperiment(mod_pos_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', posgrams=2, detailed=True, lexicalized=False, include_gp=False)
    # crossTrainExperiment(mod_pos_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', posgrams='2+', detailed=True, lexicalized=False, include_gp=False)

    # crossTrainExperiment(mod_pos_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', posgrams=1, detailed=True, lexicalized=False, include_gp=True)
    # crossTrainExperiment(mod_pos_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', posgrams=2, detailed=True, lexicalized=False, include_gp=True)
    # crossTrainExperiment(mod_pos_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', posgrams='2+', detailed=True, lexicalized=False, include_gp=True)

    # crossTrainExperiment(mod_pos_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', posgrams=1, detailed=True, lexicalized=True, include_gp=True)
    # crossTrainExperiment(mod_pos_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', posgrams=2, detailed=True, lexicalized=True, include_gp=True)
    # crossTrainExperiment(mod_pos_pcfg, models.get('linear_svm'), polarity='all', train='open_domain', posgrams='2+', detailed=True, lexicalized=True, include_gp=True)



    # crossTrainExperiment(mod_syntax, models.get('linear_svm'), polarity="all", train="open_domain", ngrams=2, posgrams=1, detailed=True)


    # ottExperiment(mod_pcfg, models.get("nb"), lexicalized=True, include_gp=False)

    # ottExperiment(mod_ngram, models.get("linear_svm"), ngrams=2)


    # ottExperiment(mod_ngram_pcfg, models.get("linear_svm"), ngrams=2, lexicalized=False, include_gp=False) #Seems to improve over bigrams
    # ottExperiment(mod_ngram_pcfg, models.get("linear_svm"), ngrams=2, lexicalized=True, include_gp=False)
    # ottExperiment(mod_ngram_pcfg, models.get("linear_svm"), ngrams=2, lexicalized=False, include_gp=True)
    # ottExperiment(mod_ngram_pcfg, models.get("linear_svm"), ngrams=2, lexicalized=True, include_gp=True)

    # ottExperiment(mod_pos_pcfg, models.get("linear_svm"), posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    # ottExperiment(mod_syntax, models.get("linear_svm"), posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    # ottExperiment(mod_ngram_pos_pcfg, models.get("linear_svm"), ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=False)
    # ottExperiment(mod_all, models.get("linear_svm"), ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=False) 
    # ottExperiment(mod_all_feature_select, models.get("linear_svm"), ngrams=1, posgrams=1, detailed=True, lexicalized=False, include_gp=False) 
    

    # ottFiveFold(mod_ngram_pcfg, models.get("linear_svm"), ngrams=2, lexicalized=False, include_gp=False) #Note: 92% Accuracy (on positive)!!!

    # ottFiveFold(mod_ngram_pcfg, models.get("linear_svm"), polarity="all", ngrams=2, lexicalized=False, include_gp=False)

    # openDomainExperiment(mod_pcfg, models.get("linear_svm"), collapse_users=True, lexicalized=False, include_gp=False)
    # openDomainExperiment(mod_pcfg, models.get("linear_svm"), collapse_users=True, lexicalized=True, include_gp=False)
    # openDomainExperiment(mod_pcfg, models.get("linear_svm"), collapse_users=True, lexicalized=False, include_gp=True)
    # openDomainExperiment(mod_pcfg, models.get("linear_svm"), collapse_users=True, lexicalized=True, include_gp=True)

    # openDomainExperiment(mod_ngram_pcfg, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=False, include_gp=False)
    # openDomainExperiment(mod_ngram_pcfg, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=True, include_gp=False)
    # openDomainExperiment(mod_ngram_pcfg, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=False, include_gp=True)
    # openDomainExperiment(mod_ngram_pcfg, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=True, include_gp=True)

    # openDomainExperiment(mod_ngram_dep_pcfg, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=False, include_gp=False)
    # openDomainExperiment(mod_ngram_dep_pcfg, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=True, include_gp=False)
    # openDomainExperiment(mod_ngram_dep_pcfg, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=False, include_gp=True)
    # openDomainExperiment(mod_ngram_dep_pcfg, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=True, include_gp=True)

    # openDomainExperiment(mod_dep_pcfg, models.get("linear_svm"), collapse_users=True, lexicalized=False, include_gp=False)
    # openDomainExperiment(mod_dep_pcfg, models.get("linear_svm"), collapse_users=True, lexicalized=True, include_gp=False)
    # openDomainExperiment(mod_dep_pcfg, models.get("linear_svm"), collapse_users=True, lexicalized=False, include_gp=True)
    # openDomainExperiment(mod_dep_pcfg, models.get("linear_svm"), collapse_users=True, lexicalized=True, include_gp=True)

    # openDomainExperiment(mod_ngram_pcfg_feature_select, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=False, include_gp=False)
    # openDomainExperiment(mod_ngram_pcfg_feature_select, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=True, include_gp=False)
    # openDomainExperiment(mod_ngram_pcfg_feature_select, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=False, include_gp=True)
    # openDomainExperiment(mod_ngram_pcfg_feature_select, models.get("linear_svm"), collapse_users=True, ngrams=2, lexicalized=True, include_gp=True)

    # ottExperiment(mod_ngram_pcfg_feature_select, models.get("linear_svm"), ngrams=2, lexicalized=False, include_gp=False)
    # ottExperiment(mod_ngram_pcfg_feature_select, models.get("linear_svm"), ngrams=2, lexicalized=True, include_gp=False)
    # ottExperiment(mod_ngram_pcfg_feature_select, models.get("linear_svm"), ngrams=2, lexicalized=False, include_gp=True)
    # ottExperiment(mod_ngram_pcfg_feature_select, models.get("linear_svm"), ngrams=2, lexicalized=True, include_gp=True)

    # openDomainFiveFold(mod_pcfg, models.get(mod_name), lexicalized=True, include_gp=False, ngrams=1)
    # ottFiveFold(mod_pcfg, models.get(mod_name), lexicalized=True, include_gp=False)

    # ottExperiment(mod_ngram, models.get('linear_svm'), polarity="all", verbose=True, ngrams=2)
    # ottFiveFold(mod_ngram, models.get('linear_svm'), polarity="all", verbose=True, ngrams=2)

    # openDomainFiveFold(mod_ngram, models.get('random_forest'), ngrams=1)
    # openDomainFiveFold(mod_ngram, models.get('sgd'), ngrams=1)
    # openDomainFiveFold(mod_ngram, models.get('sgd_l1'), ngrams=1, collapse_users=False)
    # openDomainFiveFold(mod_ngram_liwc, models.get('nb'), ngrams=2, collapse_users=True)

    # runTest(testBestOtt, "all", "ottAllPolarity1")

    # ottExperiment(mod_ngram, models.get('linear_svm'), verbose=True, ngrams=2)



    # 0.61 pos posgrams = 1 detailed = False sgd l1 regularization
    # syntax detailed = False 0.588 accuracy sgd l1 regularization
    # non_ngram 0.569 accuracy sgd
    # Dep all 0.569 accuracy sgd l1 regularization
    # Dep all 0.56 accuracy sgd
    # Dep all 0.55 accuracy linear_svm
    # crossTrainExperiment(mod_non_ngram, models.get('sgd'), train="open_domain", polarity="all", posgrams=1, detailed=True)
    # crossTrainExperiment(mod_syntax, models.get('sgd'), train="open_domain", polarity="all", posgrams=1, detailed=True)
    # crossTrainExperiment(mod_ngram_liwc, models.get('sgd'), train="open_domain", polarity="all", ngrams=2) # Best so far


    # crossTrainExperiment(mod_syntax, models.get('sgd'), train="open_domain", polarity="all", posgrams=1, detailed=True)
    # crossTrainExperiment(mod_syntax, models.get('linear_svm'), train="open_domain", polarity="all", posgrams=1, detailed=True)
    # crossTrainExperiment(mod_syntax, models.get('mlp'), train="open_domain", polarity="all", posgrams=1, detailed=True)
    # crossTrainExperiment(mod_syntax, models.get('logit'), train="open_domain", polarity="all", posgrams=1, detailed=True)


    # crossTrainExperiment(mod_ngram, models.get('sgd'), train="open_domain", polarity="all", verbose=True, ngrams=1)
    # ottExperiment(mod_pos, models.get('sgd'), posgrams=1, detailed=False, verbose=True)
    # crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", polarity="all", posgrams=1, detailed=False, verbose=True)
    # crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", polarity="all", posgrams='2+', detailed=False)
    # crossTrainExperiment(mod_syntax, models.get('linear_svm'), train="open_domain", polarity="all", posgrams=1, detailed=False)
    # crossTrainExperiment(mod_syntax, models.get('sgd'), train="open_domain", polarity="all", posgrams=2, detailed=True)

    # crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", collapse_users=True, posgrams=1, detailed=False) 
    # crossTrainExperiment(mod_ngram, models.get('sgd'), train="open_domain", collapse_users=True, ngrams=2) 

    # crossTrainExperiment(mod_ngram_liwc, models.get('sgd'), train="open_domain", collapse_users=True, ngrams=2)
    # crossTrainExperiment(mod_pos, models.get('sgd'), train="open_domain", collapse_users=True, posgrams=1, detailed=False)
    # openDomainExperiment(mod_pos, models.get('sgd'), posgrams='2+', detailed=True, collapse_users=True) 
    # openDomainExperiment(mod_ngram_liwc, models.get('linear_svm'), ngrams=2, collapse_users=True) 
    # openDomainExperiment(mod_pos, models.get('linear_svm'), collapse_users=True, posgrams=1, detailed=True)
    # openDomainExperiment(mod_pos, models.get('linear_svm'), collapse_users=True, posgrams=1, detailed=False)
    # openDomainExperiment(mod_ngram_liwc, models.get('sgd'), ngrams=2, collapse_users=True) 
    

    # openDomainExperiment(mod_dep, models.get('sgd'), collapse_users=True)

    # openDomainExperiment(mod_ngram, models.get('sgd'), ngrams=2, collapse_users=True)
    # openDomainFiveFold(mod_ngram_liwc, models.get('sgd'), ngrams=2, collapse_users=True)
    # openDomainFiveFold(mod_pos, models.get('linear_svm'), collapse_users=True, posgrams=1, detailed=True)

    ## Statements collapsed within users
    # Bigrams + 2-pos detailed SGD L2 regularization gives 0.72 accuracy
    # Bigrams + LIWC SGD l2 regularization gives 0.702 accuracy
    # Bigrams SGD l2 regularization gives 0.688 accuracy

    ## Statements not collapsed within users
    # Bigrams LIWC POS SGD 0.61
    # Bigrams LIWC SGD 0.6
    # Bigrams SGD 0.596
    # Bigrams pos 0.588
    # Bigrams 0.586
    # openDomainExperiment(mod_ngram, models.get('linear_svm'), ngrams=1, posgrams=1, detailed=True) # Best so far
import os
import LIWC
import settings
import spacy
import subprocess
import pickle
from spacy.lang.en import English
from stat_parser import Parser
from collections import Counter
from nltk.util import ngrams

from nltk.tree import Tree, ParentedTree

nlp = English()
model = spacy.load('en_core_web_sm') #Note: Can experiment with larger corpus


def LIWC_feats(text):
    input = [token.lemma_ for token in nlp(text.lower())] # Seems to do better as a split

    liwc = settings.loader.loadLIWC() # Caches LIWC after first load
    liwc_feats = liwc.count_matches(input)
    return liwc_feats

def POS_feats(text, detailed = True):
    if detailed:
        return Counter(token.tag_ for token in model(text.lower()))
    else:
        return Counter(token.pos_ for token in model(text.lower()))

def BIPOS_feats(text, detailed = True, combined=False):
    if detailed:
        bipos_list = ngrams((t.tag_ for t in model(text.lower())), 2, True, True, "<s>", "</s>")
    else:
        bipos_list = ngrams((t.pos_ for t in model(text.lower())), 2, True, True, "<s>", "</s>")

    bipos_feats = Counter(str(gram) for gram in bipos_list)

    if combined:
        bipos_feats.update(POS_feats(text, detailed))

    return bipos_feats

def DEP_feats(text):
    # Note: other experiments included head tokens)
    # return Counter((token.dep_, token.lemma_, token.head.lemma_) for token in model(text.lower()))
    return Counter(token.dep_ for token in model(text.lower()))


def trigram_featurizer(text, lemmatize=True):
    feats = bigram_featurizer(text, lemmatize)

    if lemmatize:
        trigrams_list = ngrams((t.lemma_ for t in nlp(text.lower())), 3, True, True, "<s>", "</s>")
    else:
        trigrams_list = ngrams((t.text for t in nlp(text.lower())), 3, True, True, "<s>", "</s>")

    feats.update(str(gram) for gram in trigrams_list)

    return feats

def bigram_featurizer(text, lemmatize=True):
    feats = unigram_featurizer(text, lemmatize)

    if lemmatize:
        bigrams_list = ngrams((t.lemma_ for t in nlp(text.lower())), 2, True, True, "<s>", "</s>")
    else:
        bigrams_list = ngrams((t.text for t in nlp(text.lower())), 2, True, True, "<s>", "</s>")

    # Porter Stemmer
    # bigrams_list = ngrams((stemmer.stem(t.text) for t in nlp(text.lower())), 2, True, True, "<s>", "</s>")


    feats.update(str(gram) for gram in bigrams_list)

    return feats


def unigram_featurizer(text, lemmatize=True):
    if(lemmatize): # Lemmatization
        return Counter(token.lemma_ for token in nlp(text.lower()))
    else:
        return Counter(token.text for token in nlp(text.lower())) 

    # return Counter(stemmer.stem(token.text) for token in nlp(text.lower())) # Porter Stemmer
    
    # Stemming improves precision but hurts recall, resulting in slightly lower average f1-score
    # Removing stop words hurts performance
    # Lemmatization slightly improves performance
   
def PCFG_feats_cached(text, lex, gp, dataset):

    if dataset == "both":
        res = PCFG_feats_cached_helper(text, lex, gp, "ott")
        if not res is None: return res
        res = PCFG_feats_cached_helper(text, lex, gp, "open_domain")
        if res is None:
            raise KeyError("'{}' not found in either dataset".format(text))
        return res
    else:

        lex_str = ("_no" if not lex else "") + "_lex"
        gp_str = ("_no" if not gp else "") + "_gp"
        key = dataset + lex_str + gp_str
        
        if key in settings.loader.cache:
            feats_dict = settings.loader.cache[key]
            return feats_dict[text]

        filepath = os.path.join("pcfg_parses", key + '.pickle')
        with open(filepath, 'rb') as infile:
            feats_dict = pickle.load(infile)
            settings.loader.cache[key] = feats_dict

            return feats_dict[text]

def PCFG_feats_cached_helper(text, lex, gp, dataset):
    lex_str = ("_no" if not lex else "") + "_lex"
    gp_str = ("_no" if not gp else "") + "_gp"
    key = dataset + lex_str + gp_str
    
    if key in settings.loader.cache:
        feats_dict = settings.loader.cache[key]
        if text in feats_dict:
            return feats_dict[text]
        else:
            return None

    filepath = os.path.join("pcfg_parses", key + '.pickle')
    with open(filepath, 'rb') as infile:
        feats_dict = pickle.load(infile)
        settings.loader.cache[key] = feats_dict

        if text in feats_dict:
            return feats_dict[text]
        else:
            return None

def PCFG_feats_all_types(text):
    trees = get_pcfg_trees(text)
    return {
        "no_lex_no_gp" : PCFG_feats_single_type(trees, False, False),
        "lex_no_gp" : PCFG_feats_single_type(trees, True, False),
        "no_lex_gp" : PCFG_feats_single_type(trees, False, True), 
        "lex_gp" : PCFG_feats_single_type(trees, True, True) 
    }

def PCFG_feats_single_type(trees, lex, gp):
    feats = Counter()
    for tree in trees:
        feats.update(PCFG_feats_helper(tree, lex, gp))
    return feats

def PCFG_feats(text, lex, gp):
    feats = Counter()
    trees = get_pcfg_trees(text)
    for tree in trees:
        feats.update(PCFG_feats_helper(tree, lex, gp))
    return feats

def PCFG_feats_helper(tree, lex, gp):
    if not gp:
        if lex:
            return Counter(tree.productions())
        else:
            return Counter([rule for rule in tree.productions() if '\'' not in str(rule)]) # This is inefficient. better way?

    else:
        parented = ParentedTree.convert(tree)
        for tree in parented.subtrees():
            if tree.parent():
                curLabel = tree.label()
                lineage = tree.parent().label().split('^')
                if len(curLabel.split('^')) < 2:
                    tree.set_label(curLabel + '^' + lineage[0])

        rules = parented.productions()
        newRules = []
        for rule in rules:
            split = str(rule).split(' ')
            delim = '^'
            split[2:] = [node.split(delim, 1)[0] for node in split[2:]]
            newRules.append(' '.join(split))


        if lex:
            return Counter(newRules)
            # return Counter(str(rule) for rule in newRules)
        else:
            return Counter([rule for rule in newRules if '\'' not in str(rule)])


def get_pcfg_trees(text):
    """
    Returns a list of NLTK Tree object corresponding to the parses for each sentence in the text
    """
    os.chdir("stanford-parser-full-2018-02-27")
    with open("input.txt", 'w') as infile:
        infile.write(text)

    batchpath = "lexparser_trees.bat input.txt"
    # batchpath = os.path.join("stanford-parser-full-2018-02-27", "lexparser_trees.bat")
    parses = subprocess.check_output(batchpath).decode("utf-8")
    # print(parses)
    parses = parses.replace("(())", "") # Removes empty parses from sentences that exceed max length
    # with open("outfile.txt", 'w') as outfile:
    #     outfile.write(parses)

    os.chdir("..")

    parses_split = parses.split("(ROOT")[1:] #Ignore the first split, it's empty

    # print(parses_split)

    return [Tree.fromstring("(ROOT" + parse) for parse in parses_split]
    # return parse_tree.productions()

def PCFG_feats_OLD(text, lex, gp):
    """
    DEPRECATED alternate parser version of PCFG features
    """
    tree = parser.parse(text)
    if not gp:
        if lex:
            return Counter(tree.productions())
        else:
            return Counter([rule for rule in tree.productions() if '\'' not in str(rule)]) # This is inefficient. better way?

    else:
        parented = ParentedTree.convert(tree)
        for tree in parented.subtrees():
            if tree.parent():
                curLabel = tree.label()
                lineage = tree.parent().label().split('^')
                if len(curLabel.split('^')) < 2:
                    tree.set_label(curLabel + '^' + lineage[0])

        rules = parented.productions()
        newRules = []
        for rule in rules:
            split = str(rule).split(' ')
            delim = '^'
            split[2:] = [node.split(delim, 1)[0] for node in split[2:]]
            newRules.append(' '.join(split))


        if lex:
            return Counter(newRules)
            # return Counter(str(rule) for rule in newRules)
        else:
            return Counter([rule for rule in newRules if '\'' not in str(rule)])

#####################    TESTING        ########################

def test():
    settings.init()
    text = 'The movie was great. Ultimately the sequel outshadowed the original.'
    liwcFeats = LIWC_feats(text)    # this is the only one that produces a dict object and not a Counter object
    print(type(liwcFeats))

    posFeats = POS_feats(text)
    biposFeats = BIPOS_feats(text)
    simpleposFeats = POS_feats(text, False)
    simplebiposFeats = BIPOS_feats(text, False)
    depFeats = DEP_feats(text)
    lemmatizedtrigramFeats = trigram_featurizer(text)
    trigramFeats = trigram_featurizer(text, False)

    print('####################        LIWC FEATURES        ########################')
    print(liwcFeats)
    print('\n')
    print('####################    DETAILED POS FEATURES ########################')
    print(posFeats)
    print('\n')
    print('####################    DETAILED BIPOS FEATURES ########################')
    print(biposFeats)
    print('\n')
    print('####################    DEP FEATURES ########################')
    print(depFeats)
    print('\n')
    print('####################    SIMPLE POS FEATURES ########################')
    print(simpleposFeats)
    print('\n')
    print('####################    SIMPLE BIPOS FEATURES ########################')
    print(simplebiposFeats)
    print('\n')
    print('####################    LEMMATIZED TRIGRAM FEATURES ########################')
    print(lemmatizedtrigramFeats)
    print('\n')
    print('####################    TRIGRAM FEATURES ########################')
    print(trigramFeats)

def testPCFG():
    text = 'The movie was great. Ultimately the sequel outshadowed the original.'

    for tree in parser.parse(["Jack", "saw", "telescopes"]):
        print(tree)

    # print(parser.parse_all(nlp(text.lower())))

    # print(PCFG_feats(text, lex=True, gp=False))

if __name__ == "__main__":
    test()
    # load_PCFG_parser()
    # testPCFG()





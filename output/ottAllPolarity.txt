
Model: sgd
Five-fold cross-validation all polarity
Features: [2-grams] [1-POS detailed: True] 
Features: [2-grams] [1-POS detailed: True] 
Features: [2-grams] [1-POS detailed: True] 
Features: [2-grams] [1-POS detailed: True] 
Features: [2-grams] [1-POS detailed: True] 
Accuracy: 0.875625
             precision    recall  f1-score   support

  deceptive      0.898     0.848     0.872       800
   truthful      0.856     0.904     0.879       800

avg / total      0.877     0.876     0.876      1600


Model: sgd
Five-fold cross-validation all polarity
Features: [2-grams] 
Features: [2-grams] 
Features: [2-grams] 
Features: [2-grams] 
Features: [2-grams] 
Accuracy: 0.8925
             precision    recall  f1-score   support

  deceptive      0.898     0.885     0.892       800
   truthful      0.887     0.900     0.893       800

avg / total      0.893     0.892     0.892      1600


Model: sgd
Five-fold cross-validation all polarity
Features: [2-grams] [2+-POS detailed: True] 
Features: [2-grams] [2+-POS detailed: True] 
Features: [2-grams] [2+-POS detailed: True] 
Features: [2-grams] [2+-POS detailed: True] 
Features: [2-grams] [2+-POS detailed: True] 
Accuracy: 0.87375
             precision    recall  f1-score   support

  deceptive      0.891     0.851     0.871       800
   truthful      0.858     0.896     0.877       800

avg / total      0.875     0.874     0.874      1600


Model: sgd
Five-fold cross-validation all polarity
Features: [2-grams] [DEP] 
Features: [2-grams] [DEP] 
Features: [2-grams] [DEP] 
Features: [2-grams] [DEP] 
Features: [2-grams] [DEP] 
Accuracy: 0.874375
             precision    recall  f1-score   support

  deceptive      0.887     0.858     0.872       800
   truthful      0.862     0.891     0.876       800

avg / total      0.875     0.874     0.874      1600


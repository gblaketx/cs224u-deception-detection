################################ Unigrams + DEP + POS ################################

Model: svm
Five-fold cross-validation positive polarity
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.75      0.89      0.81       400
   truthful       0.86      0.70      0.77       400

avg / total       0.80      0.79      0.79       800


Model: nb
Five-fold cross-validation positive polarity
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.81      0.96      0.88       400
   truthful       0.95      0.77      0.85       400

avg / total       0.88      0.86      0.86       800


Model: random_forest
Five-fold cross-validation positive polarity
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.85      0.89      0.87       400
   truthful       0.89      0.84      0.86       400

avg / total       0.87      0.86      0.86       800


Model: logit
Five-fold cross-validation positive polarity
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.86      0.87      0.86       400
   truthful       0.87      0.85      0.86       400

avg / total       0.86      0.86      0.86       800


Model: mlp
Five-fold cross-validation positive polarity
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.90      0.89      0.89       400
   truthful       0.89      0.90      0.90       400

avg / total       0.90      0.90      0.89       800


Model: sgd
Five-fold cross-validation positive polarity
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
Features: [1-grams] [1-POS detailed: True] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.84      0.94      0.89       400
   truthful       0.93      0.82      0.87       400

avg / total       0.89      0.88      0.88       800

################################ Unigrams + DEP + LIWC ################################

Model: svm
Five-fold cross-validation positive polarity
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.78      0.76      0.77       400
   truthful       0.76      0.79      0.77       400

avg / total       0.77      0.77      0.77       800


Model: nb
Five-fold cross-validation positive polarity
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.79      0.97      0.87       400
   truthful       0.96      0.75      0.84       400

avg / total       0.88      0.86      0.86       800


Model: random_forest
Five-fold cross-validation positive polarity
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.84      0.86      0.85       400
   truthful       0.86      0.84      0.85       400

avg / total       0.85      0.85      0.85       800


Model: logit
Five-fold cross-validation positive polarity
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.83      0.82      0.82       400
   truthful       0.82      0.83      0.83       400

avg / total       0.83      0.83      0.83       800


Model: mlp
Five-fold cross-validation positive polarity
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.89      0.89      0.89       400
   truthful       0.89      0.89      0.89       400

avg / total       0.89      0.89      0.89       800


Model: sgd
Five-fold cross-validation positive polarity
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
Features: [1-grams] [LIWC] [DEP] 
             precision    recall  f1-score   support

  deceptive       0.85      0.93      0.89       400
   truthful       0.92      0.83      0.88       400

avg / total       0.88      0.88      0.88       800


def show_most_informative_features(vectorizer, clf, n=20):
    """
    Prints features with the highest coefficient values, per class
    Adapted from https://stackoverflow.com/questions/11116697/
    how-to-get-most-informative-features-for-scikit-learn-classifiers
    ?utm_medium=organic&utm_source=google_rich_qa&utm_campaign=google_rich_qa

    #TODO: This isn't quite working properly
    """
    feature_names = vectorizer.get_feature_names()
    coefs_with_fns = sorted(zip(clf.coef_[0], feature_names))
    top = zip(coefs_with_fns[:n], coefs_with_fns[:-(n + 1):-1])
    print(" \t{}\t\t\t {}".format(*clf.classes_))
    for (coef_1, fn_1), (coef_2, fn_2) in top:
        print("\t%.4f\t%-15s\t\t%.4f\t%-15s" % (coef_1, fn_1, coef_2, fn_2))

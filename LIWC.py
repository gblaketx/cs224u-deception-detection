import numpy as np
import pandas as pd
import math
import re
import os
from collections import Counter


class LIWC():
	"""docstring for LIWC"""

	class LIWCTrie():
		"""A trie used by LIWC as an internal data structure for looking up labels"""
		class Node():

			def __init__(self):
				self.labels = set() # The LIWC labels corresponding to this node, if any
				self.children = {} # Dict mapping from letter to node
				self.wildcard = False # Boolean was to whether node is wildcard match


		def __init__(self):
			self.root = LIWC.LIWCTrie.Node()


		def addWord(self, word, label):
			self.addWordHelper(word, label, self.root)

		def addWordHelper(self, word, label, cur):
			if len(word) == 0:
				cur.labels.add(label)
				return

			if word[0] == '.':
				cur.labels.add(label)
				cur.wildcard = True
				return

			if word[0] in cur.children: # Node already exists
				self.addWordHelper(word[1:], label, cur.children[word[0]])
			else: # Create a new node
				new_node = LIWC.LIWCTrie.Node()
				cur.children[word[0]] = new_node
				self.addWordHelper(word[1:], label, new_node)

		def getLabels(self, word):
			return self.getLabelsHelper(word, self.root)


		def getLabelsHelper(self, word, cur):
			if len(word) == 0 or cur.wildcard: #Note: Can differentiate between . and .*
				return cur.labels #Note: continue search after wildcard?

			# Note: For example, apple and apples will return same labels if any label has apple.*

			if word[0] in cur.children:
				return self.getLabelsHelper(word[1:], cur.children[word[0]])
			else:
				return set()




	def __init__(self):
		super(LIWC, self).__init__()

		data_home = "data"
		liwc_path = os.path.join(data_home, 'LIWC', 'LIWC2007dictionary poster.csv')
		self.df = pd.read_csv(liwc_path)
		self.trie = LIWC.LIWCTrie()

		for col in self.df.iteritems():
			if col[0] is 'Negate' or col[0] is 'You' or col[0] is 'Certain':
				label =  "LIWC_{}".format(col[0]) #Append LIWC to differentiate key from ngrams
				for cell in col[1]:
					if type(cell) is str: 
						self.trie.addWord(cell.lower(), label) #All words lowercased
			else:
				label =  "LIWC_{Other}"
				for cell in col[1]:
					if type(cell) is str: 
						self.trie.addWord(cell.lower(), label) #All words lowercased


	# catCounts = {}
	# data_home = "data"
	# liwc_path = os.path.join(data_home, 'LIWC', 'LIWC2007dictionary poster.csv')
	# df = pd.read_csv(liwc_path)

	# change all Nans to empty strs 
	# initialize a dict mapping categories to num matches
	# for col in df.iteritems():
	# 	catCounts[col[0]] = 0
	# 	for i, cell in enumerate(col[1]):
	# 		if type(cell) is not str:
	# 			df[col[0]][i] = ''




	###################################################################
	# takes in a list of words from the text we want to featurize
	# returns a dict of
	# LIWC categories to the # times input words appear in that category 
	# PREPROCESSING REQUIRED:
	#	-remove punctuation
	#	-split string on ' '
	# def count_matches(self, input):
	# 	for cat in self.catCounts.keys():
	# 		for cell in self.df[cat]:
	# 			if not cell: continue	# no need to check all the empty strings
	# 			count = 0
	# 			exp = re.compile(cell + '$')
	# 			matches = [re.match(exp, w) for w in input if re.match(exp, w)]
	# 			count += len(matches)
	# 			self.catCounts[cat] += count
	# 	return self.catCounts


	def count_matches(self, input):
		labels = []
		for token in input:
			labels.extend(self.trie.getLabels(token))

		return Counter(labels)

	@staticmethod
	def testTrie():
		trie = LIWC.LIWCTrie()
		trie.addWord("apple", "Fruit")
		trie.addWord("app", "Computer")
		trie.addWord("apes", "Animal")
		trie.addWord("apple", "Nature")
		trie.addWord("apple.*", "Tree")
		trie.addWord("zebra", "Animal")

		print(trie.getLabels("app"), " should be {Computer}")
		print(trie.getLabels("apples"), " should be {Fruit, Nature, Tree}")
		print(trie.getLabels("apes"), " should be {Animal}")
		print(trie.getLabels("zebra"), " should be {Animal}")
		print(trie.getLabels("xkcd"), "should be {}")

	@staticmethod
	def testLIWC():
		text = "This is a test of LIWC's ability to correctly label tokens in a sentence."

		punctuation = ['.', ',', '?'] #Note: check punctuation 
		for item in punctuation:
			text = text.replace(item, ' ')
		tokens = text.split(' ')
		liwc = LIWC()
		res = liwc.count_matches(tokens)
		print(res)

if __name__ == '__main__':
	LIWC.testLIWC()